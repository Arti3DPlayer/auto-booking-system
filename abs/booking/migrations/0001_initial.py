# Generated by Django 2.0.5 on 2018-05-23 11:28

from django.conf import settings
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BookingTask',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('currency', models.CharField(choices=[('KLM', 'KLM Royal Dutch Airlines')], max_length=30)),
                ('additional_params', django.contrib.postgres.fields.jsonb.JSONField(verbose_name='Additional params')),
                ('status', models.PositiveSmallIntegerField(choices=[(0, 'In queue'), (1, 'In progress'), (2, 'Failed'), (3, 'Success')], default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(help_text='Task owner', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
        ),
    ]
