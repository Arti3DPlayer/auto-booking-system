# Generated by Django 2.0.5 on 2018-08-31 12:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('booking', '0031_ostrovokhome'),
    ]

    operations = [
        migrations.CreateModel(
            name='BookingAccount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(blank=True, max_length=50)),
                ('email', models.EmailField(max_length=254)),
                ('password', models.CharField(blank=True, max_length=50)),
                ('is_active', models.BooleanField(default=True)),
                ('object_id', models.PositiveIntegerField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
            ],
        ),
    ]
