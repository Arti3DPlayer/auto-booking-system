# Generated by Django 2.0.5 on 2018-05-23 11:55

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0002_auto_20180523_1136'),
    ]

    operations = [
        migrations.CreateModel(
            name='BookingFlightTask',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('booking_registry', models.CharField(blank=True, max_length=30, null=True)),
                ('additional_params', django.contrib.postgres.fields.jsonb.JSONField(blank=True, default={}, verbose_name='Additional params')),
                ('status', models.PositiveSmallIntegerField(choices=[(0, 'In queue'), (1, 'In progress'), (2, 'Failed'), (3, 'Success')], default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='bookingtask',
            name='additional_params',
        ),
        migrations.RemoveField(
            model_name='bookingtask',
            name='booking_registry',
        ),
        migrations.AddField(
            model_name='bookingflighttask',
            name='booking_task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='booking.BookingTask'),
        ),
        migrations.CreateModel(
            name='KLMBookingFlightTask',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
            },
            bases=('booking.bookingflighttask',),
        ),
    ]
