# Generated by Django 2.0.5 on 2018-05-31 15:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0014_auto_20180531_1153'),
    ]

    operations = [
        migrations.CreateModel(
            name='BookingHome',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.PositiveSmallIntegerField(choices=[(0, 'In queue'), (1, 'In progress'), (2, 'Failed'), (3, 'Success')], default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('city', models.CharField(max_length=255)),
                ('date_from', models.DateField()),
                ('date_to', models.DateField()),
                ('booking_task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='booking.BookingTask')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
