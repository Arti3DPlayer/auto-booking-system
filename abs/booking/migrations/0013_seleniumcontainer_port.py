# Generated by Django 2.0.5 on 2018-05-31 09:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0012_taskerror'),
    ]

    operations = [
        migrations.AddField(
            model_name='seleniumcontainer',
            name='port',
            field=models.IntegerField(default=4444),
            preserve_default=False,
        ),
    ]
