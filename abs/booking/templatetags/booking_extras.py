from django import template
from django.contrib.contenttypes.models import ContentType

from ..models import BookingBase

register = template.Library()


@register.filter(is_safe=True)
def status_badge_css_class(status):
    css_map = {
        BookingBase.TASK_IN_QUEUE_STATUS: 'badge-light',
        BookingBase.TASK_IN_PROGRESS_STATUS: 'badge-warning',
        BookingBase.TASK_FAILED_STATUS: 'badge-danger',
        BookingBase.TASK_SUCCESS_STATUS: 'badge-success',
    }
    return css_map.get(status, '')


@register.filter
def content_type(obj):
    if not obj:
        return False
    return ContentType.objects.get_for_model(obj)


@register.filter
def classname(obj):
    return obj.__class__.__name__
