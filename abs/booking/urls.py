from django.urls import path

from .endpoints import BookingTaskEndpoint
from .views import BookingTasksView, CreateBookingTaskView, GetBookingFormView, GetPersonFormView, \
    BookingTasksTableView, BookingObjectTaskErrorDetailView, GetInsuranceFormView, RenderInsuranceTemplate

app_name = 'booking'
urlpatterns = [
    path('', BookingTasksView.as_view(), name='booking_tasks'),
    path('tasks/table/', BookingTasksTableView.as_view(), name='booking_tasks_table'),
    path('add/', CreateBookingTaskView.as_view(), name='create_booking_task'),
    path('create/task/', BookingTaskEndpoint.as_view(), name='create_task'),
    path('get/person/form/', GetPersonFormView.as_view(), name='get_person_form'),
    path('get/booking/form/', GetBookingFormView.as_view(), name='get_booking_form'),
    path('get/insurance/form/', GetInsuranceFormView.as_view(), name='get_insurance_form'),
    path('render/insurance/template/<int:pk>/', RenderInsuranceTemplate.as_view(), name='render_insurance_template'),
    path('booking/object/<int:ct>/<int:pk>/',
         BookingObjectTaskErrorDetailView.as_view(),
         name='booking_object_task_errors_detail'),
]
