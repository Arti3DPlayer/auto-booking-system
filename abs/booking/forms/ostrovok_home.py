from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms import layout

from ..models import OstrovokHome
from ..booking_registry import bookingRegistry


class OstrovokHomeForm(forms.ModelForm):
    key_name = forms.ChoiceField(choices=bookingRegistry.as_choices(),
                                 widget=forms.HiddenInput(),
                                 initial='Ostrovok')

    def __init__(self, *args, **kwargs):
        super(OstrovokHomeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = layout.Layout(
            'key_name',
            'city',
            layout.Div(
              layout.HTML('Dates*')
            ),
            layout.Div(
                layout.Field('date_from', css_class='form-control', template='crispy_forms/input_without_wrapper.html'),
                layout.HTML('<span class="input-group-addon">to</span>'),
                layout.Field('date_to', css_class='form-control', template='crispy_forms/input_without_wrapper.html'),
                css_class='input-daterange input-group'
            )
        )

    class Meta:
        model = OstrovokHome
        exclude = ('booking_task', 'status', 'booking_confirmation_url', 'booking_print_url')
