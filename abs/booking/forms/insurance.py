from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms import layout

from ..models import Insurance


class InsuranceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(InsuranceForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = layout.Layout(
            layout.Div(
                layout.Div(
                    'company',
                    css_class='col-4'
                ),
                layout.Div(
                    'policyholder',
                    css_class='col-4'
                ),
                layout.Div(
                    'issue_date',
                    css_class='col-4'
                ),
                css_class='row'
            ),
            layout.Div(
              layout.HTML('Dates*')
            ),
            layout.Div(
                layout.Field('date_from', css_class='form-control', template='crispy_forms/input_without_wrapper.html'),
                layout.HTML('<span class="input-group-addon">to</span>'),
                layout.Field('date_to', css_class='form-control', template='crispy_forms/input_without_wrapper.html'),
                css_class='input-daterange input-group'
            )
        )

    class Meta:
        model = Insurance
        exclude = ('insurance_file', )
