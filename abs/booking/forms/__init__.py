from .person import PersonForm
from .klm_flight import KLMFlightForm
from .biletix_flight import BiletixFlightForm
from .booking_home import BookingHomeForm
from .ostrovok_home import OstrovokHomeForm
from .s7_flight import S7FlightForm
from .insurance import InsuranceForm
