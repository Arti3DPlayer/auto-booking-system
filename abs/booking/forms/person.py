import datetime
from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms import layout, bootstrap

from ..models import Person


class PersonForm(forms.ModelForm):
    date_of_birth = forms.DateField(required=False, widget=forms.SelectDateWidget(
        years=range(1940, datetime.date.today().year),
        attrs=({'style': 'width: 33%; display: inline-block;'})
    ))

    def __init__(self, *args, **kwargs):
        super(PersonForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = layout.Layout(
            layout.Div(
                layout.Div(
                    'first_name',
                    css_class='col-5'
                ),
                layout.Div(
                    'last_name',
                    css_class='col-5'
                ),
                layout.Div(
                    'gender',
                    css_class='col-2'
                ),
                css_class='row'
            ),
            layout.Div(
                layout.Div(
                    'date_of_birth',
                    css_class='col-5 date_of_birth_row'
                ),
                css_class='row'
            )
        )

    class Meta:
        model = Person
        exclude = ('content_object', 'content_type', 'object_id')
