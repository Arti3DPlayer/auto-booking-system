from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms import layout

from ..models import BiletixFlight
from ..booking_registry import bookingRegistry


class BiletixFlightForm(forms.ModelForm):
    key_name = forms.ChoiceField(choices=bookingRegistry.as_choices(),
                                 widget=forms.HiddenInput(),
                                 initial='Biletix')
    city_from = forms.ChoiceField(choices=(('Москва', 'Москва'), ), initial='Москва')

    def __init__(self, *args, **kwargs):
        super(BiletixFlightForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = layout.Layout(
            'key_name',
            layout.Div(
                layout.Div(
                    'city_from',
                    css_class='col-6'
                ),
                layout.Div(
                    'city_to',
                    css_class='col-6'
                ),
                css_class='row'
            ),
            layout.Div(
              layout.HTML('Dates*')
            ),
            layout.Div(
                layout.Field('date_from', css_class='form-control', template='crispy_forms/input_without_wrapper.html'),
                layout.HTML('<span class="input-group-addon">to</span>'),
                layout.Field('date_to', css_class='form-control', template='crispy_forms/input_without_wrapper.html'),
                css_class='input-daterange input-group'
            )
        )

    class Meta:
        model = BiletixFlight
        exclude = ('ticket', 'booking_task', 'status')
