import time
import logging
from datetime import datetime
from django.core.files.base import ContentFile
from urllib.parse import urlencode

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException, WebDriverException, UnexpectedAlertPresentException, StaleElementReferenceException

from .base import BaseBooking, bookingRegistry
from ..modules.workflow_handler import WorkflowHandler, WorkflowStatus, RuleSet, Rule, Priority,\
    PlainText, CaseLess, Mandatory, Url, Regex, document_handler, WorkflowError
from ..modules.exceptions import UserDataErrorException

from ..utils import generate_random_email, \
    generate_random_date_of_birth, \
    generate_random_passport_number, generate_random_passport_date_of_expiry, generate_random_phone_number

logger = logging.getLogger(__name__)


class S7FlightWorkflowHandler(WorkflowHandler):

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('ibe.s7.ru/air', PlainText, CaseLess, Mandatory, Url))], invocation_limit=1)
    def main_page(self):
        logger.info("Main page")
        logger.info(self.browser.current_url)

        logger.info("Enter city from")
        departure_airport_el = self.__wait_element(xpath_selector='.//*[@id="departure_airport_0"]')
        departure_airport_el.send_keys(self.context['city_from'])
        time.sleep(3)
        logger.info("Select first suggest city from")
        autocomplete_from_el = self.__wait_element(xpath_selector='.//*[@id="autocomplete_from"]')
        first_suggest_el = self.__wait_element(xpath_selector='.//ul/li/a', parent=autocomplete_from_el)
        self.__click_on_element(elem=first_suggest_el)

        logger.info("Enter city to")
        return_airport_el = self.__wait_element(xpath_selector='.//*[@id="return_airport_0"]')
        return_airport_el.send_keys(self.context['city_to'])
        time.sleep(3)
        logger.info("Select first suggest city to")
        autocomplete_to_el = self.__wait_element(xpath_selector='.//*[@id="autocomplete_to"]')
        first_suggest_el = self.__wait_element(xpath_selector='.//ul/li/a', parent=autocomplete_to_el)
        self.__click_on_element(elem=first_suggest_el)

        logger.info("Select date from")
        dep_date_el = self.__wait_element(xpath_selector='.//input[@id="dep_date_0"]')
        dep_date_el.send_keys(self.context['date_from'].strftime('%d.%m.%Y'))

        logger.info("Select date to")
        return_date_el = self.__wait_element(xpath_selector='.//input[@id="return_date_0"]')
        return_date_el.send_keys(self.context['date_to'].strftime('%d.%m.%Y'))

        logger.info("Click on city from element to hide date calendar")
        self.__click_on_element(elem=departure_airport_el)
        time.sleep(3)

        logger.info("Select persons count")
        adults_counter_plus_el = self.__wait_element(xpath_selector='.//div[@id="adults_counter"]/span[@data-qa="plus_counter"]')

        for _ in range(1, len(self.context['persons'])):
            self.__click_on_element(elem=adults_counter_plus_el)
            time.sleep(1)

        submit_button_el = self.__wait_element(xpath_selector='.//button[@id="submitButton"]')
        self.__click_on_element(elem=submit_button_el)

        self.__wait_overlay(xpath_selector='.//div[@id="csb_advert_banner"]/div[@class="preloader_overlay"]')

        time.sleep(10)

        modal_error_el = self.__wait_element(xpath_selector='.//div[@id="no_results"]', raise_error=False)
        if modal_error_el:
            hidden_modal_input = modal_error_el.find_element_by_xpath('.//input[@id="g_a_errors"]')
            if hidden_modal_input:
                message = hidden_modal_input.get_attribute('data-error-message')
            else:
                message = 'Flight not found'

            workflow_error = WorkflowError(message=message)
            self.context['errors'].append(workflow_error)
            raise UserDataErrorException

        submit_button_el = self.__wait_element(xpath_selector='.//button[@id="submitButton"]')
        self.__click_on_element(elem=submit_button_el)

        self.__wait_overlay(xpath_selector='.//div[@id="csb_advert_banner"]/div[@class="preloader_overlay"]')

        time.sleep(10)

        flight_insurance_el = self.__wait_element(xpath_selector='.//div[@data-qa="item_insurance" and @data-type="FLIGHT"]')
        self.__click_on_element(elem=flight_insurance_el)

        time.sleep(10)

        submit_button_el = self.__wait_element(xpath_selector='.//button[@id="submitButton"]')
        self.__click_on_element(elem=submit_button_el)

        self.__wait_overlay(xpath_selector='.//div[@id="csb_advert_banner"]/div[@class="preloader_overlay"]')

        time.sleep(10)

        submit_button_el = self.__wait_element(xpath_selector='.//button[@id="submitButton"]')
        self.__click_on_element(elem=submit_button_el)

        self.__wait_overlay(xpath_selector='.//div[@id="csb_advert_banner"]/div[@class="preloader_overlay"]')

        self.__wait_overlay(xpath_selector='.//div[@class="js_page_locker"]')

        time.sleep(3)

        for index, person in enumerate(self.context['persons']):
            self.__fill_passangers_data(index=index,
                                        first_name=person['first_name'],
                                        last_name=person['last_name'],
                                        gender=person['gender'],
                                        date_of_birth=person['date_of_birth'],
                                        passport_number=person['passport_number'],
                                        passport_date_of_expiry=person['passport_date_of_expiry'])

        main_person = self.context['persons'][0]

        logger.info("Passenger Email")
        email_el = self.__wait_element(xpath_selector='//input[@id="contacts_eml"]')
        email_el.send_keys(generate_random_email(first_name=main_person['first_name'],
                                                 last_name=main_person['last_name']))

        phone_el = self.__wait_element(xpath_selector='//input[@id="contacts_phoneNo"]')

        logger.info("Passenger phone")
        formatted_phone = main_person['phone'].replace('+7', '')
        phone_el.send_keys(formatted_phone)

        logger.info("Pay later button")
        pay_later_el = self.__wait_element(
            './/ul[@data-qa="paymentTabs_paymentMethodsBlock"]/li[contains(text(), "Оплата позже")]')
        self.__click_on_element(elem=pay_later_el)
        time.sleep(1)

        logger.info("Confirm terms")
        terms_text_el = self.__wait_element('.//div[@class="terms-text"]')
        self.__click_on_element(elem=terms_text_el)
        time.sleep(1)

        submit_button_el = self.__wait_element(xpath_selector='//button[@id="submitButton"]')
        self.__click_on_element(elem=submit_button_el)

        self.__wait_overlay(xpath_selector='.//div[@id="csb_advert_banner"]/div[@class="preloader_overlay"]')

        time.sleep(3)

        self.__wait_element(xpath_selector='.//h2[contains(text(), "Детали брони")]')
        time.sleep(5)

        flock_api_popup_el = self.__wait_element(
            xpath_selector='.//div[contains(@class, "flockapi-overlay flockapi-overlay_popup")]', raise_error=False)

        if flock_api_popup_el:
            logger.info('Remove flockapi popup element')
            self.browser.execute_script("""var element = arguments[0];element.parentNode.removeChild(element);""",
                                        flock_api_popup_el)
        else:
            logger.info('Remove flockapi popup does not exist')

        self.context['results'] = {}
        # remove unnecessary blocks to make print()
        self.browser.execute_script("""
        var element, i;
        var elements_selectors_to_remove = [
            ".js_cookie_policy_wrapper", 
            "header",
            "nav",
            ".alerts",
            ".banner",
            ".flockapi-overlay",
            ".planning-trip",
            "footer",
            "#_hj_feedback_container"
        ]
        for (i=0; i<elements_selectors_to_remove.length; i++) {
            element = document.querySelector(elements_selectors_to_remove[i]);
            if (element)
                element.parentNode.removeChild(element);
        }
        
        var passanger_details_els = document.querySelectorAll('.passenger-details');  
        
        if (passanger_details_els.length>1) {
            passanger_details_els[1].parentNode.removeChild(passanger_details_els[1]);
        }
        
        var additional_info_els = document.querySelectorAll('.passenger-details .additional-info');
        
        for (i=0; i<additional_info_els.length; i++) {
            additional_info_els[i].parentNode.removeChild(additional_info_els[i]);
        }
        
        element = document.querySelector("#page-inner");
        if (element)
            element.style.padding = 0;
            
        element = document.querySelector("aside");
        if (element)
            element.style.height = 'auto';
            
        var styles_els = document.getElementsByTagName("link");
        
        for (var i=0; i<styles_els.length; i++) {
            if (styles_els[i].getAttribute('href')) {
                styles_els[i].setAttribute("href", 'https://ibe.s7.ru'+styles_els[i].getAttribute('href'));
            }
        }    
        
        """)
        elem = self.browser.find_element(By.XPATH, '//*')
        source_code = elem.get_attribute('outerHTML')
        # https://stackoverflow.com/a/14975912/1786016
        # add styles and script for print html
        source_code = source_code.replace('</body>', '<style type="text/css" media="print">'
                                                     '@page {size: auto; margin: 0; }</style>'
                                                     '<script>window.print();</script></body>')

        self.context['results']['source_code'] = source_code

        self.status = WorkflowStatus.WorkflowSuccess

    def __fill_passangers_data(self, index, first_name, last_name, gender, date_of_birth, passport_number, passport_date_of_expiry):
        logger.info(f"Passenger {index} gender")
        if gender == 'mr':
            gender_el = self.__wait_element(xpath_selector=f'.//input[@name="passengersRequest.passengers[{index}].gender" and @value="MALE"]/../label')
        else:
            gender_el = self.__wait_element(xpath_selector=f'.//input[@name="passengersRequest.passengers[{index}].gender" and @value="FEMALE"]/../label')
        self.__click_on_element(elem=gender_el)

        logger.info(f"Passenger {index} first name")
        first_name_el = self.__wait_element(xpath_selector=f'.//input[@id="givenName{index}"]')
        first_name_el.send_keys(first_name)

        logger.info(f"Passenger {index} last name")
        last_name_el = self.__wait_element(xpath_selector=f'.//input[@name="passengersRequest.passengers[{index}].name.lastName"]')
        last_name_el.send_keys(last_name)

        logger.info(f"Passenger {index} date of birth")
        date_of_birth_el = self.__wait_element(
            xpath_selector=f'.//input[@name="passengersRequest.passengers[{index}].document.dateOfBirth"]')
        date_of_birth_el.send_keys(date_of_birth.strftime('%d.%m.%Y'))

        logger.info(f"Passenger {index} passport number")
        document_number_el = self.__wait_element(xpath_selector=f'.//input[@id="documentNumber{index}"]')
        document_number_el.send_keys(passport_number)

        logger.info(f"Passenger {index} pasport expiration date")
        date_of_expiry_el = self.__wait_element(
            xpath_selector=f'.//input[@name="passengersRequest.passengers[{index}].document.expirationDate"]')
        date_of_expiry_el.send_keys(passport_date_of_expiry.strftime('%d.%m.%Y'))

    def __wait_overlay(self, xpath_selector):
        logger.info(f'Waiting overlay {xpath_selector}')
        try:
            WebDriverWait(self.browser, 10).until(
                EC.visibility_of_element_located(
                    (By.XPATH, xpath_selector))
            )
            overlay_el = self.browser.find_element_by_xpath(xpath_selector)
            while overlay_el.is_displayed():
                time.sleep(0.2)
        except (StaleElementReferenceException, TimeoutException, NoSuchElementException):
            pass

    def __wait_element(self, xpath_selector, parent=None, timeout=15, raise_error=True):
        if not parent:
            parent = self.browser
        logger.info(f'Locating {xpath_selector}')
        try:
            WebDriverWait(self.browser, timeout).until(
                EC.visibility_of_element_located(
                    (By.XPATH, xpath_selector))
            )
        except (TimeoutException, NoSuchElementException) as e:
            if raise_error:
                logger.error(
                    f'{xpath_selector} not found. Abort.',
                    exc_info=True)
                raise e
            return None

        return parent.find_element_by_xpath(xpath_selector)

    def __click_on_element(self, elem, retries=2):
        logger.info(f'Click on element {elem}')
        try:
            elem.click()
        except Exception as e:
            if retries > 0:
                retries = retries-1
                logger.warning(f'Click failed, retry({retries})')
                time.sleep(3)
                self.__click_on_element(elem=elem, retries=retries)
            else:
                raise e


@bookingRegistry.register
class S7Flight(BaseBooking):
    key_name = 'S7'
    display_name = 'S7'
    main_page = 'https://ibe.s7.ru/air'
    workflow_handler = S7FlightWorkflowHandler
    form_template_name = 'booking/s7_flight_form.html'

    @property
    def model_class(self):
        from ..models import S7Flight
        return S7Flight

    @property
    def form_class(self):
        from ..forms import S7FlightForm
        return S7FlightForm

    @property
    def serializer_class(self):
        from ..serializers import S7FlightSerializer
        return S7FlightSerializer

    def make_booking(self, obj):
        from ..models import Person

        profile = webdriver.FirefoxProfile()
        profile.set_preference("print.always_print_silent", True)
        profile.set_preference("print.show_print_progress", False)
        profile.set_preference("browser.link.open_newwindow", 1)
        profile.set_preference('permissions.default.image', 2)
        # https://support.mozilla.org/en-US/questions/1167673
        profile.set_preference("browser.tabs.remote.autostart", False)
        profile.set_preference("browser.tabs.remote.autostart2", False)
        profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', False)
        proxy = self.get_proxy()

        browser = webdriver.Remote(command_executor=f'http://hub:4444/wd/hub',
                                   desired_capabilities=DesiredCapabilities.FIREFOX,
                                   browser_profile=profile,
                                   proxy=proxy)
        obj.session_id = browser.session_id
        obj.save()

        try:
            browser.get(self.main_page)
        except WebDriverException as e:
            browser.quit()
            raise e

        persons = []
        gender_map = {
            Person.GENDER_MR: 'mr',
            Person.GENDER_MS: 'ms',
            Person.GENDER_MRS: 'ms',
            Person.GENDER_MISS: 'ms'
        }

        for person in obj.persons.all():
            persons.append({
                'first_name': person.first_name,
                'last_name': person.last_name,
                'gender': gender_map[person.gender],
                'date_of_birth': generate_random_date_of_birth(),
                'passport_number': generate_random_passport_number(),
                'passport_date_of_expiry': generate_random_passport_date_of_expiry(),
                'phone': generate_random_phone_number()
            })

        worker = self.workflow_handler(browser, {
            'city_from': obj.city_from,
            'city_to': obj.city_to,
            'date_from': obj.date_from,
            'date_to': obj.date_to,
            'persons': persons,
        })

        worker.dispatch_while_working()
        browser.quit()
        return worker

    def save_data(self, obj, worker_context):
        logger.info("Saving data")
        flight_ticket_file = ContentFile(worker_context['results']['source_code'])
        flight_ticket_file.name = "flight_ticket_file.html"
        obj.ticket = flight_ticket_file
        obj.save()
        logger.info("Data saved")

