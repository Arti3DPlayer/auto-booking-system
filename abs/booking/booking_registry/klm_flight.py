import time
import logging
from datetime import datetime
from django.core.files.base import ContentFile
from urllib.parse import urlencode

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException, WebDriverException, UnexpectedAlertPresentException, StaleElementReferenceException

from .base import BaseBooking, bookingRegistry
from ..modules.workflow_handler import WorkflowHandler, WorkflowStatus, RuleSet, Rule, Priority,\
    PlainText, CaseLess, Mandatory, Url, Regex, document_handler, WorkflowError
from ..modules.exceptions import UserDataErrorException

from ..utils import generate_random_email, generate_random_date_of_birth, generate_random_phone_number

logger = logging.getLogger(__name__)


class KLMFlightWorkflowHandler(WorkflowHandler):

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('www.klm.com/travel/ru_ru/plan_and_book/index.htm', PlainText, CaseLess, Mandatory, Url))], invocation_limit=1)
    def booking_page(self):
        logger.info("Main page")
        logger.info(self.browser.current_url)
        self.__close_cookie_modal()

        booking_calendar_a_elem = self.__wait_element(xpath_selector=".//a[contains(@href, '/booking/book_a_flight/')]")

        self.__click_on_element(elem=booking_calendar_a_elem)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('/booking/book_a_flight/', PlainText, CaseLess, Mandatory, Url))], invocation_limit=1)
    def book_a_flight_page(self):
        logger.info("Booking page")
        logger.info(self.browser.current_url)

        time.sleep(5)

        try:
            cookie_agree_button = self.__wait_element(xpath_selector='.//button[contains(@class, "cookiebar-agree-button-agree")]')
            self.__click_on_element(elem=cookie_agree_button)
        except (TimeoutException, NoSuchElementException):
            pass

        # Select city from
        from_input_elem = self.__wait_element(xpath_selector=".//input[@id='bf-station-picker-input-origin']")

        from_input_elem.send_keys(self.context['city_from'].title())

        try:
            origin_city_div_elem = self.__wait_element(xpath_selector=f".//div[@id='origin-listbox']//div[@class='bf-station-picker__listbox__option__title' and contains(., '{self.context['city_from'].title()} -')]")
        except (TimeoutException, NoSuchElementException) as e:
            workflow_error = WorkflowError(
                message=f'Город отправления - {self.context["city_from"]} не был найден на сайте. '
                        f'Проверьте его наличие')
            self.context['errors'].append(workflow_error)
            raise UserDataErrorException

        self.__click_on_element(elem=origin_city_div_elem)

        # Select city to
        to_input_elem = self.__wait_element(xpath_selector=".//input[@id='bf-station-picker-input-destination']")

        to_input_elem.send_keys(self.context['city_to'].title())

        try:
            destination_city_elem = self.__wait_element(xpath_selector=f".//div[@id='destination-listbox']//div[@class='bf-station-picker__listbox__option__title' and contains(., '{self.context['city_to'].title()} -')]")
        except (TimeoutException, NoSuchElementException) as e:
            workflow_error = WorkflowError(
                message=f'Город прибытия - {self.context["city_to"]} не был найден на сайте. Проверьте его наличие')
            self.context['errors'].append(workflow_error)
            raise UserDataErrorException

        self.__click_on_element(elem=destination_city_elem)

        # Select persons count

        passangers_count_elem = self.__wait_element(xpath_selector=".//input[@id='bf-pax-selector__input-pax']/..")

        self.__click_on_element(elem=passangers_count_elem)

        passangers_count_input_elem = self.__wait_element(xpath_selector=".//input[@id='pax-selector-1']")

        passangers_count_input_elem.send_keys(len(self.context['persons']))

        # Select date from
        date_from_wrapper_elem = self.__wait_element(xpath_selector=".//input[@class='bf-overview-pane__field__input bf-date-picker__field__input']/..")

        self.__click_on_element(elem=date_from_wrapper_elem)

        calendar_div_el = self.__wait_element(xpath_selector=".//div[@class='bf-details-pane__component bf-date-picker-calendar']")

        formatted_date_from = self.context['date_from'].strftime("%Y-%m-%d")
        date_from_el = self.__wait_element(xpath_selector=f".//button[@data-bf-a11y-datecalendarmonth-id='{formatted_date_from}']",
                                           parent=calendar_div_el)
        self.__click_on_element(elem=date_from_el)

        # Select date to
        date_to_wrapper_elem = self.__wait_element(xpath_selector=".//input[@class='bf-overview-pane__field__input bf-date-picker__field__input' and contains(@aria-label, 'Выберите дату возвращения')]/..")

        self.__click_on_element(elem=date_to_wrapper_elem)

        calendar_div_el = self.__wait_element(xpath_selector=".//div[@class='bf-details-pane__component bf-date-picker-calendar']")

        formatted_date_to = self.context['date_to'].strftime("%Y-%m-%d")
        date_to_el = self.__wait_element(xpath_selector=f".//button[@data-bf-a11y-datecalendarmonth-id='{formatted_date_to}']",
                                         parent=calendar_div_el)
        self.__click_on_element(elem=date_to_el)

        # Click on submit button
        submit_button_el = self.__wait_element(xpath_selector=".//button[@type='submit' and ./span[contains(text(), 'Смотреть предложения')]]")
        self.__click_on_element(elem=submit_button_el)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('newSearchWeb=true', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def search_page(self):
        logger.info("Search booking_registry page")
        logger.info(self.browser.current_url)
        # select from booking_registry
        try:
            self.__select_flight()
        except (TimeoutException, NoSuchElementException) as e:
            return

        # select to booking_registry
        try:
            self.__select_flight()
        except (TimeoutException, NoSuchElementException) as e:
            return

        button_next = self.__wait_element(xpath_selector="//button[@id = 'bf-continue-button']")
        self.__click_on_element(elem=button_next)

        button_next = self.__wait_element(xpath_selector="//button[@id = 'bf-continue-button']")
        self.__click_on_element(elem=button_next)

    @document_handler(Priority.High,
                      [RuleSet(Rule('newSearchWeb=true', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=6)
    def ajax_wait_search_page(self):
        logger.info("Ajax wait search page")
        logger.info(self.browser.current_url)
        time.sleep(5)

    @document_handler(Priority.High,
                      [RuleSet(Rule('/ams/ancillaries/', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=6)
    def ajax_wait_ancillaries_page(self):
        logger.info("Ajax wait ancillaries page")
        logger.info(self.browser.current_url)
        time.sleep(5)

    @document_handler(Priority.Low,
                      [RuleSet(Rule('/plan_and_book/booking/book_a_flight/index.htm', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=6)
    def plan_and_book_index_page(self):
        logger.info("Plan adn book index page")
        logger.info(self.browser.current_url)

        try:
            button_next = self.__wait_element(xpath_selector="//button[@id = 'bf-continue-button']")
            self.__click_on_element(elem=button_next)
        except (TimeoutException, NoSuchElementException):
            logger.warning("Contiune button not found, maybe it is ajax page. Waiting..")
            time.sleep(5)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('/ps/booking/products', PlainText, CaseLess, Mandatory, Url))], invocation_limit=1)
    def ams_ancillaryries(self):
        logger.info("Customize page")
        logger.info(self.browser.current_url)

        button_next = self.__wait_element(
            xpath_selector="//button[@type = 'submit' and contains(@class, 'g-forms-next')]")
        self.__click_on_element(elem=button_next)
        time.sleep(5)

    @document_handler(Priority.High,
                      [RuleSet(
                          Rule('/create-order', PlainText, CaseLess, Mandatory, Url))], invocation_limit=3)
    @document_handler(Priority.High,
                      [RuleSet(
                          Rule('https://www.klm.com/ams/checkout-beta/ticket?backUrl=', PlainText, CaseLess, Mandatory, Url))], invocation_limit=3)
    def create_order_ajax_redirect(self):
        logger.info("Create order ajax redirect page")
        logger.info(self.browser.current_url)
        time.sleep(5)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('/passenger', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def passenger_data_page(self):
        logger.info("Personal data page")
        logger.info(self.browser.current_url)

        self.__close_cookie_modal()

        for index, person in enumerate(self.context['persons']):
            self.__fill_passangers_data(index=index,
                                        first_name=person['first_name'],
                                        last_name=person['last_name'],
                                        gender=person['gender'],
                                        date_of_birth=person['date_of_birth'])

        main_person = self.context['persons'][0]

        email_address = generate_random_email(first_name=main_person["first_name"], last_name=main_person["last_name"])

        email_input_el = self.__wait_element(xpath_selector='//input[@id="passengerField_1000_emailAddress"]')
        email_input_el.send_keys(email_address)

        phone_number_code_option_el = self.__wait_element(
            xpath_selector=".//select[@id='passengerField_1000_phoneNumberFirstCountry']/option[text()='Российская Федерация (+7)']")
        self.__click_on_element(elem=phone_number_code_option_el)

        phone = generate_random_phone_number()
        formatted_phone = phone[2:]

        phone_number_input_el = self.__wait_element(
            xpath_selector=".//input[@id='passengerField_1000_phoneNumberFirst']")
        phone_number_input_el.send_keys(formatted_phone)

        checkout_submit_el = self.__wait_element(xpath_selector='.//button[@id = "checkout-step-submit"]')
        self.__click_on_element(elem=checkout_submit_el)

    @document_handler(Priority.High,
                      [RuleSet(
                          Rule('/passenger', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=6)
    def passenger_ajax_wait_page(self):
        logger.info("Personal data ajax page")
        time.sleep(5)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('/payment', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def payment_page(self):
        logger.info("Payment page")
        logger.info(self.browser.current_url)

        payment_input_el = self.__wait_element(xpath_selector='//span[text() = "Оплатить в офисе KLM"]/../../input',
                                               raise_error=False)
        time.sleep(2)

        if not payment_input_el:
            payment_input_el = self.__wait_element(xpath_selector='//span[contains(text(), "Оплатите через службу поддержки")]/../../input')

        self.__click_on_element(elem=payment_input_el)

        logger.info('Locating //input[@id = "acceptConditions"]')
        try:
            WebDriverWait(self.browser, 15).until(
                EC.visibility_of_element_located(
                    (By.XPATH, '//input[@id = "acceptConditions"]'))
            )
        except (TimeoutException, NoSuchElementException):
            logger.error(
                '//input[@id = "acceptConditions"] not found. Abort',
                exc_info=True)
            return

        accept_conditions_el = self.browser.find_element(By.XPATH, '//input[@id = "acceptConditions"]')
        self.__click_on_element(elem=accept_conditions_el)
        time.sleep(1)
        logger.info('Locating //button[@id = "payButton"]')
        try:
            WebDriverWait(self.browser, 5).until(
                EC.visibility_of_element_located(
                    (By.XPATH, '//button[@id = "payButton"]'))
            )
        except (TimeoutException, NoSuchElementException):
            logger.error(
                '//button[@id = "payButton"] not found. Abort',
                exc_info=True)
            return

        pay_button_el = self.browser.find_element(By.XPATH, '//button[@id = "payButton"]')
        self.__click_on_element(elem=pay_button_el)
        time.sleep(15)

    @document_handler(Priority.High,
                      [RuleSet(
                          Rule('/payment', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=6)
    def payment_ajax_waiting_page(self):
        logger.info("Payment ajax page")
        logger.info(self.browser.current_url)
        time.sleep(5)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('overview.xhtml', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def mytrip_page(self):
        logger.info("My trip page")
        logger.info(self.browser.current_url)
        time.sleep(5)
        # Remove all target="_blank" attr from links
        self.browser.execute_script(
            """Array.from(document.querySelectorAll('a[target="_blank"]')).forEach(link => link.removeAttribute('target'));""")

        logger.info("Waiting modal window disappear")
        try:
            WebDriverWait(self.browser, 5).until(
                EC.visibility_of_element_located(
                    (By.XPATH, './/*[@id="mmb-waiting-bg"]'))
            )
            overlay_el = self.browser.find_element_by_xpath('.//*[@id="mmb-waiting-bg"]')
            while overlay_el and overlay_el.is_displayed():
                time.sleep(0.2)
                try:
                    close_elem = self.browser.find_element(By.XPATH, './/div[contains(@class, "mmb-waiting-close")]/a')
                    close_elem.click()
                except WebDriverException:
                    pass

        except (StaleElementReferenceException, TimeoutException, NoSuchElementException):
            pass
        time.sleep(5)
        print_a_el = self.__wait_element(xpath_selector='.//a[@data-label-key="header.print.flight.details"]')
        self.__click_on_element(elem=print_a_el)
        time.sleep(10)

    @document_handler(Priority.High,
                      [RuleSet(
                          Rule('overview.xhtml', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=6)
    def mytrip_ajax_page(self):
        logger.info("My trip ajax page")
        time.sleep(5)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('bookingConfirmation', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=6)
    def booking_confirmation_ajax_waiting_page(self):
        logger.info("Booking confirmation page")
        logger.info(self.browser.current_url)
        time.sleep(5)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('/ams/mytrip/printBooking', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def print_booking_page(self):
        elem = self.browser.find_element(By.XPATH, '//*')
        source_code = elem.get_attribute('outerHTML')

        source_code = source_code.replace('img src="/ams', 'img src="https://www.klm.com/ams')
        source_code = source_code.replace('href="/ams', 'href="https://www.klm.com/ams')

        self.context['results'] = {
            'source_code': source_code
        }

        self.status = WorkflowStatus.WorkflowSuccess

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('/error', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def error_checkout_page(self):
        logger.info("Error checkout page")
        logger.info(self.browser.current_url)

        workflow_error = WorkflowError(
            message=f'Ошибка при заполнении личных данных. Проверьте правильность')
        self.context['errors'].append(workflow_error)
        raise UserDataErrorException

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('/passenger#error-notification', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def passenger_error_incorrect_form_page(self):
        logger.info("Passanger fill form error page")
        logger.info(self.browser.current_url)
        self.status = WorkflowStatus.WorkflowFailure

    @document_handler(Priority.Lowest,
                      [RuleSet(
                          Rule('www.klm.com', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=5)
    def passenger_error_incorrect_form_page(self):
        logger.info("Unhandled ajax page")
        logger.info(self.browser.current_url)
        time.sleep(3)

    def __close_cookie_modal(self):
        try:
            time.sleep(2)
            cookie_agree_button = self.__wait_element(xpath_selector='.//button[contains(@class, "cookiebar-agree-button-agree")]')
            self.__click_on_element(elem=cookie_agree_button)
        except (TimeoutException, NoSuchElementException):
            pass

    def __select_flight(self):
        try:
            # check for message that no flights available
            WebDriverWait(self.browser, 5).until(
                EC.visibility_of_element_located(
                    (By.XPATH, ".//h4[contains(@class, 'bf-flight-list__no-flights')]"))
            )
            no_flights_elem = self.browser.find_element_by_xpath(".//h4[contains(@class, 'bf-flight-list__no-flights')]")
            workflow_error = WorkflowError(
                message=no_flights_elem.text)
            self.context['errors'].append(workflow_error)
            raise UserDataErrorException
        except (TimeoutException, NoSuchElementException):
            pass

        logger.info("Locating .//ul[@class='bf-flight-list']")
        try:
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(
                    (By.XPATH, ".//ul[@class='bf-flight-list']"))
            )
        except (TimeoutException, NoSuchElementException) as e:
            logger.error(".//ul[@class='bf-flight-list'] not found. Abort", exc_info=True)
            raise e

        tickets_li = self.browser.find_elements(By.XPATH, './/ul[@class="bf-flight-list"]/li')
        for li in tickets_li:
            li.click()
            time.sleep(5)
            return

    def __fill_passangers_data(self, index, first_name, last_name, gender, date_of_birth):
        index += 1000
        logger.info(f"Locating .//input[@id = 'passengerField_{index}_firstName']")
        try:
            WebDriverWait(self.browser, 5).until(
                EC.visibility_of_element_located(
                    (By.XPATH, f".//input[@id = 'passengerField_{index}_firstName']"))
            )
        except (TimeoutException, NoSuchElementException) as e:
            logger.error(f".//input[@id = 'passengerField_{index}_firstName'] not found. Abort", exc_info=True)
            raise e

        self.browser.find_element(By.XPATH, f".//input[@id = 'passengerField_{index}_firstName']").send_keys(first_name)

        logger.info(f"Locating .//input[@id = 'passengerField_{index}_lastName']")
        try:
            WebDriverWait(self.browser, 5).until(
                EC.visibility_of_element_located(
                    (By.XPATH, f".//input[@id = 'passengerField_{index}_lastName']"))
            )
        except (TimeoutException, NoSuchElementException) as e:
            logger.error(f".//input[@id = 'passengerField_{index}_lastName'] not found. Abort", exc_info=True)
            raise e

        self.browser.find_element(By.XPATH, f".//input[@id = 'passengerField_{index}_lastName']").send_keys(last_name)

        logger.info(f".//select[@id = 'passengerField_{index}_title']/option[@value='{gender}']")
        try:
            WebDriverWait(self.browser, 5).until(
                EC.visibility_of_element_located(
                    (By.XPATH, f".//select[@id = 'passengerField_{index}_title']/option[@value='{gender}']"))
            )
        except (TimeoutException, NoSuchElementException) as e:
            logger.error(f".//select[@id = 'passengerField_{index}_title']/option[@value='{gender}'] not found. Abort", exc_info=True)
            raise e

        self.browser.find_element(By.XPATH, f".//select[@id = 'passengerField_{index}_title']/option[@value='{gender}']").click()

        try:
            day_select_el = self.__wait_element(
                xpath_selector=f'.//checkout-form-validation-group[@form-name="passengerField_{index}_dateOfBirth"]//select[@name="day"]',
                timeout=10)
            Select(day_select_el).select_by_value(f'string:{date_of_birth.day:02}')
            month_select_el = self.__wait_element(
                xpath_selector=f'.//checkout-form-validation-group[@form-name="passengerField_{index}_dateOfBirth"]//select[@name="month"]',
                timeout=10)
            Select(month_select_el).select_by_value(f'string:{date_of_birth.month:02}')
            year_select_el = self.__wait_element(
                xpath_selector=f'.//checkout-form-validation-group[@form-name="passengerField_{index}_dateOfBirth"]//select[@name="year"]',
                timeout=10)
            Select(year_select_el).select_by_value(f'string:{date_of_birth.year}')
        except (TimeoutException, NoSuchElementException) as e:
            logger.info("Date of birth elements for some cities not appear. It is fine.")

    def __wait_element(self, xpath_selector, parent=None, timeout=15, raise_error=True):
        if not parent:
            parent = self.browser
        logger.info(f'Locating {xpath_selector}')
        try:
            WebDriverWait(self.browser, timeout).until(
                EC.visibility_of_element_located(
                    (By.XPATH, xpath_selector))
            )
        except (TimeoutException, NoSuchElementException) as e:
            if raise_error:
                logger.error(
                    f'{xpath_selector} not found. Abort.',
                    exc_info=True)
                raise e
            return None

        return parent.find_element_by_xpath(xpath_selector)

    def __click_on_element(self, elem, retries=2):
        logger.info(f'Click on element {elem}')
        try:
            elem.click()
        except Exception as e:
            if retries > 0:
                retries = retries-1
                logger.warning(f'Click failed, retry({retries})')
                time.sleep(3)
                self.__click_on_element(elem=elem, retries=retries)
            else:
                raise e


@bookingRegistry.register
class KLMFlight(BaseBooking):
    key_name = 'KLM'
    display_name = 'KLM Royal Dutch Airlines'
    main_page = 'https://www.klm.com/travel/ru_ru/plan_and_book/index.htm'
    workflow_handler = KLMFlightWorkflowHandler
    form_template_name = 'booking/klm_flight_form.html'

    @property
    def model_class(self):
        from ..models import KLMFlight
        return KLMFlight

    @property
    def form_class(self):
        from ..forms import KLMFlightForm
        return KLMFlightForm

    @property
    def serializer_class(self):
        from ..serializers import KLMFlightSerializer
        return KLMFlightSerializer

    def make_booking(self, obj):
        from ..models import Person

        profile = webdriver.FirefoxProfile()
        profile.set_preference("print.always_print_silent", True)
        profile.set_preference("print.show_print_progress", False)
        profile.set_preference("browser.link.open_newwindow", 1)
        profile.set_preference('permissions.default.image', 2)
        # https://support.mozilla.org/en-US/questions/1167673
        profile.set_preference("browser.tabs.remote.autostart", False)
        profile.set_preference("browser.tabs.remote.autostart2", False)
        profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', False)
        proxy = self.get_proxy()

        browser = webdriver.Remote(command_executor=f'http://hub:4444/wd/hub',
                                   desired_capabilities=DesiredCapabilities.FIREFOX,
                                   browser_profile=profile,
                                   proxy=proxy)
        obj.session_id = browser.session_id
        obj.save()
        browser.get(self.main_page)

        persons = []
        gender_map = {
            Person.GENDER_MR: 'string:MR',
            Person.GENDER_MS: 'string:MS',
            Person.GENDER_MRS: 'string:MRS',
            Person.GENDER_MISS: 'string:MISS'
        }

        for person in obj.persons.all():
            persons.append({
                'first_name': person.first_name,
                'last_name': person.last_name,
                'gender': gender_map[person.gender],
                'date_of_birth': generate_random_date_of_birth()
            })

        worker = self.workflow_handler(browser, {
            'city_from': obj.city_from,
            'city_to': obj.city_to,
            'date_from': obj.date_from,
            'date_to': obj.date_to,
            'persons': persons,
        })

        worker.dispatch_while_working()
        browser.quit()
        return worker

    def save_data(self, obj, worker_context):
        logger.info("Saving data")
        flight_ticket_file = ContentFile(worker_context['results']['source_code'])
        flight_ticket_file.name = "flight_ticket_file.html"
        obj.ticket = flight_ticket_file
        obj.save()
        logger.info("Data saved")

