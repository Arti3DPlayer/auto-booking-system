from .base import bookingRegistry
from .klm_flight import KLMFlight
from .booking_home import BookingHome
from .biletix_flight import BiletixFlight
from .s7_flight import S7Flight
from .ostrovok_home import OstrovokHome
