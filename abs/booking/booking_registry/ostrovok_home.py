import re
import time
import logging
import random
from datetime import datetime
from urllib.parse import parse_qsl, urlencode, urlparse, urlunparse

from django.core.files.base import ContentFile

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException, \
    StaleElementReferenceException, WebDriverException

from .base import BaseBooking, bookingRegistry
from ..modules.workflow_handler import WorkflowHandler, WorkflowStatus, RuleSet, Rule, Priority,\
    PlainText, CaseLess, Mandatory, Url, Regex, document_handler, WorkflowError
from ..modules.exceptions import UserDataErrorException

from ..credit_card_numbers_generator import generate_card
from ..utils import generate_random_phone_number, generate_random_email, generate_random_address, set_query_field

logger = logging.getLogger(__name__)


class OstrovokHomeWorkflowHandler(WorkflowHandler):

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('ostrovok.ru', PlainText, CaseLess, Mandatory,
                                    Url))], invocation_limit=1)
    def index_page(self):
        logger.info("Index page")
        logger.info(self.browser.current_url)

        input_div_wrapper = self.__wait_element(xpath_selector='.//div[contains(@class, "zen-homepage-searchform-destination")]')
        self.__click_on_element(elem=input_div_wrapper)

        input_city = self.__wait_element(xpath_selector='.//input[@class="zen-destination-input"]')
        input_city.send_keys(self.context['city'])
        time.sleep(1)
        suggest_row = self.__wait_element(xpath_selector='.//div[@class="suggest-row"]')
        self.__click_on_element(elem=suggest_row)

        zen_checkbox = self.__wait_element(xpath_selector='.//div[@class="zen-compareto-styled-checkbox"]')
        self.__click_on_element(elem=zen_checkbox)

        zen_button = self.__wait_element(xpath_selector='.//div[@class="zen-homepage-searchform-submit"]')
        self.__click_on_element(elem=zen_button)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('ostrovok.ru/hotel/', PlainText, CaseLess, Mandatory,
                                    Url))], invocation_limit=1)
    def hotel_search_result_page(self):
        logger.info("Hotel page without filters")
        logger.info(self.browser.current_url)

        guests_count = str(len(self.context['persons']))
        date_from_formatted = self.context['date_from'].strftime("%d.%m.%Y")
        date_to_formatted = self.context['date_to'].strftime("%d.%m.%Y")

        new_url = self.browser.current_url
        new_url = set_query_field(new_url, 'payment', 'hotel.freecancellation', replace=True)
        new_url = set_query_field(new_url, 'type_group', 'hotels', replace=True)
        new_url = set_query_field(new_url, 'guests', guests_count, replace=True)
        new_url = set_query_field(new_url, 'sort', 'price.asc', replace=True)
        new_url = set_query_field(new_url, 'dates', f'{date_from_formatted}-{date_to_formatted}', replace=True)
        self.set_dependency_status('SEARCH_FILTERS', True)
        self.browser.get(new_url)
        time.sleep(5)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('ostrovok.ru/hotel/', PlainText, CaseLess, Mandatory,
                                    Url))], invocation_limit=1, dependencies=['SEARCH_FILTERS'])
    def hotel_search_result_with_filters_page(self):
        logger.info("Hotel page")
        logger.info(self.browser.current_url)
        time.sleep(5)

        ready_chart = self.__wait_element(
            xpath_selector='.//div[contains(@class, "zen-serpresult-chart")]//div[contains(@class, "zen-piechart-ready")]',
            timeout=120,
            raise_error=False
        )

        time.sleep(10)

        if not ready_chart:
            workflow_error = WorkflowError(
                message=f'There are no hotels with following criteria. '
                        f'Try other dates')
            self.context['errors'].append(workflow_error)
            raise UserDataErrorException

        available_hotels_links = []
        retries = 5

        while retries > 0:
            try:
                for elem in self.browser.find_elements_by_xpath('.//a[@class="zen-hotelcard-address"]'):
                    available_hotels_links.append(f'{elem.get_attribute("href")}')
                retries = 0
            except StaleElementReferenceException:
                time.sleep(10)
                retries = retries - 1

        logger.info(f'Found {len(available_hotels_links)} links')

        self.context['available_hotels_links'] = available_hotels_links[:5]
        self.set_dependency_status('HOTEL_DETAILS', True)

        self.__next_hotel()
        time.sleep(5)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('ostrovok.ru/hotel/', PlainText, CaseLess, Mandatory,
                                    Url))], invocation_limit=10, dependencies=['HOTEL_DETAILS'])
    def hotel_detail_page(self):
        logger.info("Hotel detail page")
        logger.info(self.browser.current_url)
        time.sleep(5)
        self.browser.execute_script('window.onbeforeunload = undefined;')
        time.sleep(1)

        popup = self.__wait_element(xpath_selector='.//div[@class="zen-fullscreenpopup"]', raise_error=False)
        if popup:
            close_popup = self.__wait_element(xpath_selector='.//div[@class="zen-fullscreenpopup-close-icon"]',
                                              parent=popup)
            self.__click_on_element(elem=close_popup)
            time.sleep(1)

        room_active_div = self.__wait_element(
            xpath_selector='.//div[contains(@class, "zen-roomspagerate-is-active")]',
            raise_error=False)

        if room_active_div:
            book_button = self.__wait_element(
                xpath_selector='.//a[@class="zen-roomspagerate-button"]',
                parent=room_active_div,
                raise_error=False)
            if book_button:
                self.__click_on_element(elem=book_button)
                return
            else:
                logger.warning('book_button not found. Trying another link')
        else:
            logger.warning('room_active_div not found. Trying another link')

        self.__next_hotel()

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('ostrovok.ru/orders/reserve/', PlainText, CaseLess, Mandatory,
                                    Url))], invocation_limit=10)
    def hotel_order_page(self):
        logger.info("Hotel order page")
        logger.info(self.browser.current_url)
        time.sleep(5)
        self.browser.execute_script('window.onbeforeunload = undefined;')
        time.sleep(1)

        error_exist_el = self.__wait_element(xpath_selector='.//div[@class="zen-form-field-errormessage"]',
                                             raise_error=False)

        time.sleep(1)
        if error_exist_el:
            logger.warning('Error in order form page')

        if self.__modal_window_that_is_room_not_available_exist():
            self.__next_hotel()
            return

        main_person = self.context['persons'][0]
        email_address = generate_random_email(first_name=main_person["first_name"],
                                              last_name=main_person["last_name"])
        phone = generate_random_phone_number()

        main_form = self.__wait_element(xpath_selector='.//div[@class="zen-booking-contacts-form-main"]')

        email_input_el = self.__wait_element(
            xpath_selector='.//input[@name="email" and @class="zen-form-field-input"]',
            parent=main_form,
            raise_error=False
        )
        if email_input_el:
            email_input_el.clear()
            email_input_el.send_keys(email_address)

        phone_input_el = self.__wait_element(
            xpath_selector='.//input[@name="phone" and @class="zen-form-field-input"]',
            parent=main_form
        )
        phone_input_el.clear()
        phone_input_el.send_keys(phone)

        name_input = self.__wait_element(
            xpath_selector='.//input[@name="room0.0FirstName" and @class="zen-form-field-input"]',
            parent=main_form
        )
        name_input.clear()
        name_input.send_keys(main_person['first_name'])

        last_name_input = self.__wait_element(
            xpath_selector='.//input[@name="room0.0LastName" and @class="zen-form-field-input"]',
            parent=main_form
        )
        last_name_input.clear()
        last_name_input.send_keys(main_person['last_name'])

        if len(self.context['persons']) > 1:
            logger.info('Fill guests info')
            toggle_all_guests_el = self.__wait_element(
                xpath_selector='.//div[@class="zen-booking-contacts-form-room-toggle zen-booking-contacts-form-room-toggle-show"]',
                raise_error=False
            )
            if toggle_all_guests_el:
                self.__click_on_element(elem=toggle_all_guests_el)

            time.sleep(2)
            guests_div = self.__wait_element(
                xpath_selector='.//div[@class="zen-booking-contacts-form-room-guests"]'
            )
            for index, person in enumerate(self.context['persons'][1:]):
                first_name_el = self.__wait_element(
                    xpath_selector=f'.//input[@name="room0.{index}FirstName"]',
                    parent=guests_div
                )
                first_name_el.send_keys(person['first_name'])

                last_name_el = self.__wait_element(
                    xpath_selector=f'.//input[@name="room0.{index}LastName"]',
                    parent=guests_div
                )
                last_name_el.send_keys(person['last_name'])

        continue_button = self.__wait_element(
            xpath_selector='.//div[@class="zen-booking-stepbar-continue"]',
            raise_error=False
        )
        if continue_button:
            self.__click_on_element(elem=continue_button)

            time.sleep(2)

        if self.__modal_window_that_is_room_not_available_exist():
            self.__next_hotel()
            return

        no_package_elem = self.__wait_element(xpath_selector='.//label[@for="carepackage-no"]', raise_error=False)
        if no_package_elem:
            self.__click_on_element(elem=no_package_elem)

            time.sleep(1)

            continue_button = self.__wait_element(xpath_selector='.//div[@class="zen-booking-stepbar-continue"]',
                                                      raise_error=False)
            if continue_button:
                self.__click_on_element(elem=continue_button)

            time.sleep(1)

        credit_card_number, cvv = generate_card(card_type='visa16')

        card_number_elem = self.__wait_element(
            xpath_selector='.//input[@name="card_number"]',
            raise_error=False
        )
        if card_number_elem:
            card_number_elem.clear()
            card_number_elem.send_keys(credit_card_number)

        card_secure_code_elem = self.__wait_element(
            xpath_selector='.//input[@name="card_secure_code"]',
            raise_error=False
        )
        if card_secure_code_elem:
            card_secure_code_elem.clear()
            card_secure_code_elem.send_keys(cvv)

        expire_cc_month = f'{random.randint(1, 12):02}'
        expire_cc_year = f'{random.randint(2022, 2026)}'
        expire_cc_year_formatted = expire_cc_year[2:]

        expire_cc_elem = self.__wait_element(
            xpath_selector='(.//div[@class="zen-form-expiration-fields"]//input)[1]',
            raise_error=False
        )
        if expire_cc_elem:
            expire_cc_elem.clear()
            expire_cc_elem.send_keys(expire_cc_month)

        expire_cc_year_elem = self.__wait_element(
            xpath_selector='(.//div[@class="zen-form-expiration-fields"]//input)[2]',
            raise_error=False
        )
        if expire_cc_year_elem:
            expire_cc_year_elem.clear()
            expire_cc_year_elem.send_keys(expire_cc_year_formatted)

        continue_button = self.__wait_element(
            xpath_selector='.//button[@class="zen-booking-creditcard-card-submit"]',
            raise_error=False
        )
        if continue_button:
            self.__click_on_element(elem=continue_button)

        if self.__modal_window_that_is_room_not_available_exist():
            self.__next_hotel()
        time.sleep(5)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('ostrovok.ru/orders/status/', PlainText, CaseLess, Mandatory,
                                    Url))], invocation_limit=30)
    def order_status_page(self):
        logger.info("Order status page")
        logger.info(self.browser.current_url)
        time.sleep(10)
        self.browser.execute_script('window.onbeforeunload = undefined;')
        time.sleep(1)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('ostrovok.ru/orders/success/', PlainText, CaseLess, Mandatory,
                                    Url))], invocation_limit=1)
    def order_success_page(self):
        logger.info("Order success page")
        logger.info(self.browser.current_url)
        time.sleep(1)
        self.browser.execute_script('window.onbeforeunload = undefined;')
        time.sleep(1)

        self.context['results'] = {}
        self.context['results']['booking_confirmation_url'] = self.browser.current_url

        components = urlparse(self.browser.current_url)
        m = re.search('\/orders\/success\/(.*)\/', components.path)
        order_id = m.group(1)

        print_url = f'https://ostrovok.ru/orders/print/{order_id}/?{components.query}&jslang=ru&mode=voucher'

        self.context['results']['booking_print_url'] = print_url

        self.status = WorkflowStatus.WorkflowSuccess

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('error=not_allowed', PlainText, CaseLess, Mandatory,
                                    Url))], invocation_limit=10)
    def order_not_allowed_page(self):
        logger.info("Hotel is not responsive page")
        logger.info(self.browser.current_url)
        time.sleep(5)
        self.__next_hotel()

    def __modal_window_that_is_room_not_available_exist(self):
        error_modal_window_title = self.__wait_element(
            xpath_selector='.//div[@class="zen-bookerror-title" and contains(text(), "Этот номер недоступен")]',
            raise_error=False,
            timeout=10
        )
        if error_modal_window_title:
            logger.warning('Hotel room is not available')
            return True
        return False

    def __next_hotel(self):
        if len(self.context['available_hotels_links']) > 0:
            hotel_link = self.context['available_hotels_links'].pop(0)
            logger.warning('Next hotel')
            self.browser.get(hotel_link)
            time.sleep(5)
        else:
            logger.warning('Hotels not found')
            workflow_error = WorkflowError(
                message=f'Cant book hotels or they not been found. '
                        f'Try other dates')
            self.context['errors'].append(workflow_error)
            raise UserDataErrorException

    def __wait_overlay(self, xpath_selector):
        logger.info(f'Waiting overlay {xpath_selector}')
        try:
            WebDriverWait(self.browser, 5).until(
                EC.visibility_of_element_located(
                    (By.XPATH, xpath_selector))
            )
            overlay_el = self.browser.find_element_by_xpath(xpath_selector)
            while overlay_el.is_displayed():
                time.sleep(0.2)
        except (StaleElementReferenceException, TimeoutException, NoSuchElementException):
            pass

    def __wait_element(self, xpath_selector, parent=None, timeout=15, raise_error=True):
        if not parent:
            parent = self.browser
        logger.info(f'Locating {xpath_selector}')
        try:
            WebDriverWait(self.browser, timeout).until(
                EC.visibility_of_element_located(
                    (By.XPATH, xpath_selector))
            )
        except (TimeoutException, NoSuchElementException) as e:
            if raise_error:
                logger.error(
                    f'{xpath_selector} not found. Abort.',
                    exc_info=True)
                raise e
            return None

        return parent.find_element_by_xpath(xpath_selector)

    def __click_on_element(self, elem, retries=2):
        logger.info(f'Click on element {elem}')
        try:
            elem.click()
        except WebDriverException as e:
            if retries > 0:
                retries = retries-1
                logger.warning(f'Click failed, retry({retries})')
                time.sleep(3)
                self.__click_on_element(elem=elem, retries=retries)
            else:
                raise e


@bookingRegistry.register
class OstrovokHome(BaseBooking):
    key_name = 'Ostrovok'
    display_name = 'Ostrovok'
    main_page = 'https://ostrovok.ru/?lang=ru'
    workflow_handler = OstrovokHomeWorkflowHandler
    form_template_name = 'booking/ostrovok_home_form.html'

    @property
    def model_class(self):
        from ..models import OstrovokHome
        return OstrovokHome

    @property
    def form_class(self):
        from ..forms import OstrovokHomeForm
        return OstrovokHomeForm

    @property
    def serializer_class(self):
        from ..serializers import OstrovokHomeSerializer
        return OstrovokHomeSerializer

    def make_booking(self, obj):
        from ..models import Person

        profile = webdriver.FirefoxProfile()
        profile.set_preference("print.always_print_silent", True)
        profile.set_preference("print.show_print_progress", False)
        profile.set_preference("browser.link.open_newwindow", 1)
        # https://support.mozilla.org/en-US/questions/1167673
        profile.set_preference("browser.tabs.remote.autostart", False)
        profile.set_preference("browser.tabs.remote.autostart2", False)
        profile.set_preference('permissions.default.image', 2)
        profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', False)

        proxy = self.get_proxy()
        browser = webdriver.Remote(command_executor='http://hub:4444/wd/hub',
                                   desired_capabilities=DesiredCapabilities.FIREFOX,
                                   browser_profile=profile,
                                   proxy=proxy)
        obj.session_id = browser.session_id
        obj.save()

        try:
            browser.get(self.main_page)
        except WebDriverException as e:
            browser.quit()
            raise e

        persons = []
        gender_map = {
            Person.GENDER_MR: 'mr',
            Person.GENDER_MS: 'ms',
            Person.GENDER_MRS: 'ms',
            Person.GENDER_MISS: 'ms'
        }

        for person in obj.persons.all():
            persons.append({
                'first_name': person.first_name,
                'last_name': person.last_name,
                'gender': gender_map[person.gender],
            })

        worker = self.workflow_handler(browser, {
            'city': obj.city,
            'date_from': obj.date_from,
            'date_to': obj.date_to,
            'persons': persons
        })

        worker.dispatch_while_working()
        browser.quit()
        return worker

    def save_data(self, obj, worker_context):
        logger.info("Saving data")
        obj.booking_confirmation_url = worker_context['results']['booking_confirmation_url']
        obj.booking_print_url = worker_context['results']['booking_print_url']
        obj.save()
        logger.info("Data saved")
