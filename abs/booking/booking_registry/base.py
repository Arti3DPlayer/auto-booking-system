import logging
import requests

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.proxy import Proxy as BrowserProxy, ProxyType as BrowserProxyType

from abs.contrib.registry import Registry

from ..modules.exceptions import ProxyNotFoundException

logger = logging.getLogger(__name__)

bookingRegistry = Registry(key_name='key_name', display_name='display_name')


class BaseBooking:
    @property
    def key_name(self):
        raise NotImplementedError

    @property
    def display_name(self):
        raise NotImplementedError

    @property
    def workflow_handler(self):
        """
        Class that handles booking process
        :return:
        """
        raise NotImplementedError

    @property
    def main_page(self):
        """
        Main page is using as entry point for workflow handler
        :return:
        """
        raise NotImplementedError

    @property
    def model_class(self):
        """
        Model class where to save current booking info
        :return:
        """
        raise NotImplementedError

    @property
    def form_class(self):
        """
        Form class that using to create booking task
        :return:
        """
        raise NotImplementedError

    @property
    def form_template_name(self):
        """
        Form template path for creating booking task
        :return:
        """
        raise NotImplementedError

    @property
    def serializer_class(self):
        """
        Serializer class for create booking task
        :return:
        """
        raise NotImplementedError

    def make_booking(self, obj):
        """
        This method should call workflowhandler class
        :param obj: Object from database that should contain required information
        :return: workflowhandler worker
        """
        raise NotImplementedError

    def save_data(self, obj, worker_context):
        """
        This method should save data based on workflow context to database
        :param obj: Object from database
        :param worker_context: worker context object
        :return:
        """
        raise NotImplementedError

    def get_proxy(self):
        from abs.proxy.models import Proxy

        proxies = Proxy.objects.filter(status=Proxy.WORKING_STATUS,
                                       proxy_type=Proxy.PROXY_TYPE_WEB).order_by('?')
        proxies = list(proxies)
        while proxies:
            proxy = proxies.pop()
            logger.info(f"Using proxy: {proxy}")
            try:
                resp = requests.get('https://www.google.com/',
                                    timeout=6.1,
                                    proxies=dict(http=str(proxy),
                                                 https=str(proxy)))
                resp.raise_for_status()
                return BrowserProxy({
                    'httpProxy': f'{proxy.ip_address}:{proxy.port}',
                    'sslProxy':  f'{proxy.ip_address}:{proxy.port}',
                    'noProxy': ''
                })
            except requests.RequestException:
                logger.warning(f"Proxy: {proxy} seems not working, try another")
                if not proxies:
                    logger.error(f"Working proxy not found")
        raise ProxyNotFoundException
