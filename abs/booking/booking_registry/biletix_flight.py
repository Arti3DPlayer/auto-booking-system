import time
import logging
from datetime import datetime
from django.core.files.base import ContentFile
from urllib.parse import urlencode

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException, WebDriverException, UnexpectedAlertPresentException, StaleElementReferenceException, ElementClickInterceptedException

from .base import BaseBooking, bookingRegistry
from ..modules.workflow_handler import WorkflowHandler, WorkflowStatus, RuleSet, Rule, Priority,\
    PlainText, CaseLess, Mandatory, Url, Regex, document_handler, WorkflowError
from ..modules.exceptions import UserDataErrorException

from ..utils import generate_random_email, \
    generate_random_date_of_birth, \
    generate_random_passport_number, generate_random_passport_date_of_expiry, generate_random_phone_number

logger = logging.getLogger(__name__)


class BiletixFlightWorkflowHandler(WorkflowHandler):

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('biletix.ru', PlainText, CaseLess, Mandatory, Url))], invocation_limit=1)
    def main_page(self):
        logger.info("Main page")
        logger.info(self.browser.current_url)

        direction_type_el = self.__wait_element(xpath_selector='.//div[@class="directype"]//span[contains(@class, "rt")]')
        self.__click_on_element(elem=direction_type_el)

        time.sleep(5)

        city_from_el = self.__wait_element(xpath_selector='.//input[@id="depart1"]')
        city_from_el.send_keys(self.context['city_from'])

        time.sleep(2)

        city_to_el = self.__wait_element(xpath_selector='.//input[@id="arrival1"]')
        city_to_el.send_keys(self.context['city_to'])

        time.sleep(2)

        # Open calendar cause Failed to decode response from marionette
        # So we do this hack to input date
        date_from_view_els = self.browser.find_elements(by=By.XPATH, value='.//div[contains(@class, "dateto_view") and @for="dateto1"]')
        for date_from_view_el in date_from_view_els:
            self.browser.execute_script("""
            var element = arguments[0];
            element.remove();
            """, date_from_view_el)
            time.sleep(1)
        date_from_el = self.__wait_element(xpath_selector='.//input[@id="dateto1"]')
        self.browser.execute_script("""
                var element = arguments[0];
                element.removeAttribute("readonly");
                var new_element = element.cloneNode(true);
                element.parentNode.replaceChild(new_element, element);
                """, date_from_el)
        time.sleep(1)
        date_from_el = self.__wait_element(xpath_selector='.//input[@id="dateto1"]')
        formatted_date_from = self.context['date_from'].strftime("%d.%m.%Y")
        self.__click_on_element(elem=date_from_el)
        time.sleep(1)
        date_from_el.send_keys(formatted_date_from)
        time.sleep(2)

        date_to_view_els = self.browser.find_elements(by=By.XPATH,
                                                      value='.//div[contains(@class, "dateback_view") and @for="dateback1"]')
        for date_to_view_el in date_to_view_els:
            self.browser.execute_script("""
                    var element = arguments[0];
                    element.remove();
                    """, date_to_view_el)
            time.sleep(1)
        date_to_el = self.__wait_element(xpath_selector='.//input[@id="dateback1"]')
        self.browser.execute_script("""
                        var element = arguments[0];
                        element.removeAttribute("readonly");
                        var new_element = element.cloneNode(true);
                        element.parentNode.replaceChild(new_element, element);
                        """, date_to_el)
        time.sleep(1)
        date_to_el = self.__wait_element(xpath_selector='.//input[@id="dateback1"]')
        formatted_date_to = self.context['date_to'].strftime("%d.%m.%Y")
        self.__click_on_element(elem=date_to_el)
        time.sleep(1)
        date_to_el.send_keys(formatted_date_to)

        time.sleep(2)
        passengers_count = len(self.context['persons'])
        adult_span_el = self.__wait_element(
            xpath_selector=f'.//span[contains(@class, "nums") and @type="adult"]//span[text()="{passengers_count}"]')
        self.__click_on_element(elem=adult_span_el)
        time.sleep(3)
        submit_btn_el = self.__wait_element(xpath_selector='.//button[@id="form_order_submit"]')
        self.set_dependency_status('AJAX_MAIN_PAGE_WAIT', True)
        self.__click_on_element(elem=submit_btn_el)

    @document_handler(Priority.Low,
                      [RuleSet(Rule('biletix.ru', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=5,
                      dependencies=['AJAX_MAIN_PAGE_WAIT'])
    def main_ajax_page(self):
        logger.info("Main ajax page")
        logger.info(self.browser.current_url)
        time.sleep(10)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('offer__', PlainText, CaseLess, Mandatory, Url))], invocation_limit=1)
    def offer_page(self):
        logger.info("Offer page")
        logger.info(self.browser.current_url)
        self.set_dependency_status('AJAX_MAIN_PAGE_WAIT', False)
        time.sleep(5)
        select_flight_btn_el = self.__wait_element(
            xpath_selector='.//input[contains(@class, "biletix__btn") and @type="submit"]',
            raise_error=False
        )

        if not select_flight_btn_el:
            workflow_error = WorkflowError(
                message='Рейсы на эти даты не были найдены.')
            self.context['errors'].append(workflow_error)
            raise UserDataErrorException

        self.set_dependency_status('AJAX_OFFER_PAGE_WAIT', True)
        self.__click_on_element(elem=select_flight_btn_el)

    @document_handler(Priority.Low,
                      [RuleSet(Rule('offer__', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=5,
                      dependencies=['AJAX_OFFER_PAGE_WAIT'])
    def offer_ajax_page(self):
        logger.info("Offer ajax page")
        logger.info(self.browser.current_url)
        time.sleep(10)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('personal_data__', PlainText, CaseLess, Mandatory, Url))], invocation_limit=1)
    def personal_data_page(self):
        logger.info("Personal data page")
        logger.info(self.browser.current_url)

        self.set_dependency_status('AJAX_OFFER_PAGE_WAIT', False)

        for index, person in enumerate(self.context['persons']):
            self.__fill_passangers_data(index=index,
                                        first_name=person['first_name'],
                                        last_name=person['last_name'],
                                        passport_number=person['passport_number'],
                                        gender=person['gender'],
                                        date_of_birth=person['date_of_birth'],
                                        passport_date_of_expiry=person['passport_date_of_expiry'])

        time.sleep(10)

        main_person = self.context['persons'][0]
        passengers_count = len(self.context['persons'])

        email_address = generate_random_email(first_name=main_person["first_name"], last_name=main_person["last_name"])

        email_input_el = self.__wait_element(xpath_selector=f'.//input[@id="CTCDATA_CTC_MAIL_{passengers_count}"]')
        email_input_el.send_keys(email_address)

        formatted_phone = main_person['phone']

        phone_number_input_el = self.__wait_element(
            xpath_selector=f'.//input[@id="CTCDATA_CTC_PHONE_{passengers_count}"]')
        self.browser.execute_script("""
                                var element = arguments[0];
                                element.value = "";
                                element.removeAttribute("placeholder");
                                var new_element = element.cloneNode(true);
                                element.parentNode.replaceChild(new_element, element);
                                """, phone_number_input_el)
        time.sleep(1)
        phone_number_input_el = self.__wait_element(
            xpath_selector=f'.//input[@id="CTCDATA_CTC_PHONE_{passengers_count}"]')
        self.__click_on_element(elem=phone_number_input_el)
        phone_number_input_el.send_keys(formatted_phone)
        time.sleep(1)
        btn_el = self.__wait_element(xpath_selector=f'.//input[@id="button_forward_personal_data"]')
        self.set_dependency_status('AJAX_PERSONAL_DATA_PAGE_WAIT', True)
        self.__click_on_element(elem=btn_el)

    @document_handler(Priority.Low,
                      [RuleSet(Rule('personal_data__', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=5,
                      dependencies=['AJAX_PERSONAL_DATA_PAGE_WAIT'])
    def personal_data_ajax_page(self):
        logger.info("Personal data ajax page")
        logger.info(self.browser.current_url)
        time.sleep(10)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('precommit__', PlainText, CaseLess, Mandatory, Url))], invocation_limit=1)
    def precommit_page(self):
        logger.info("Precommit page")
        logger.info(self.browser.current_url)

        self.set_dependency_status('AJAX_PERSONAL_DATA_PAGE_WAIT', False)

        svyaznoy_el = self.__wait_element(
            xpath_selector='.//div[@id="paymethods_group_1"]//span[@class="paysystem_name" and contains(text(), "В салонах связи")]//..',
            raise_error=False
        )

        if not svyaznoy_el:
            workflow_error = WorkflowError(
                message='Оплата в салоне "Связной" не была найдена.')
            self.context['errors'].append(workflow_error)
            raise UserDataErrorException

        self.__click_on_element(elem=svyaznoy_el)

        btn_el = self.__wait_element(xpath_selector=f'.//div[@id="price_container_ticket"]', timeout=30)
        self.set_dependency_status('AJAX_PRECOMMIT_PAGE_WAIT', True)
        self.__click_on_element(elem=btn_el)

    @document_handler(Priority.Low,
                      [RuleSet(Rule('precommit__', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=10,
                      dependencies=['AJAX_PRECOMMIT_PAGE_WAIT'])
    def precommit_ajax_page(self):
        logger.info("Precommit ajax page")
        logger.info(self.browser.current_url)
        time.sleep(10)

    @document_handler(Priority.Low,
                      [RuleSet(Rule('progress__', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=5)
    def progress_ajax_page(self):
        logger.info("Progress ajax page")
        logger.info(self.browser.current_url)
        time.sleep(10)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('finish__', PlainText, CaseLess, Mandatory, Url))], invocation_limit=1)
    def finish_page(self):
        logger.info("Finish page")
        logger.info(self.browser.current_url)

        self.set_dependency_status('AJAX_PRECOMMIT_PAGE_WAIT', False)

        print_a_el = self.__wait_element(xpath_selector='.//a[contains(text(), "Распечатать")]')
        onclick_text = print_a_el.get_attribute("onclick")
        # inspired from there to do not use regex
        # https://stackoverflow.com/a/6883228/1786016
        onclick_text = onclick_text.replace("window.open('", '')
        onclick_text = onclick_text.replace("')", '')
        logger.info(onclick_text)
        self.browser.execute_script(f"""
                                        var element = arguments[0];
                                        element.setAttribute('href', "{onclick_text}")
                                        element.removeAttribute("onclick");
                                        element.removeAttribute("target");
                                        var new_element = element.cloneNode(true);
                                        element.parentNode.replaceChild(new_element, element);
                                        """, print_a_el)
        time.sleep(1)
        print_a_el = self.__wait_element(xpath_selector='.//a[contains(text(), "Распечатать")]')

        click_retires = 5
        while click_retires:
            try:
                print_a_el.click()
                break
            except ElementClickInterceptedException:
                time.sleep(5)
                logger.warning("Cannot click on link. flockapi-overlay existed. Removing it")
                overlay_el = self.browser.find_element(by=By.XPATH,
                                                       value='.//div[contains(@class, "flockapi-overlay")]')
                if overlay_el:
                    self.browser.execute_script("""
                                var element = arguments[0];
                                element.parentNode.removeChild(element);
                                """, overlay_el)
                    time.sleep(1)
                click_retires -= 1

        time.sleep(5)

    @document_handler(Priority.Low,
                      [RuleSet(Rule('finish__', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=5)
    def progress_ajax_page(self):
        logger.info("Finish ajax page")
        logger.info(self.browser.current_url)
        time.sleep(5)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('order_print.php', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def ticket_page(self):
        logger.info("Ticket page")
        logger.info(self.browser.current_url)
        self.browser.execute_script("""
            var statusDivs = document.getElementsByClassName("status");
            if (statusDivs.length > 0) { 
                var statusDiv = statusDivs[0]; 
                var currentHTML = statusDiv.innerHTML; 
                var parts = currentHTML.split('<br>');
                statusDiv.innerHTML = parts[1];
            }
            var addInfoDivs = document.getElementsByClassName("add_info");
            for (var i=0; i<addInfoDivs.length; i++) { 
                addInfoDivs[i].parentNode.removeChild(addInfoDivs[i]);
            }
            var passangersDivs = document.getElementsByClassName("passengers");
            if (passangersDivs.length > 0) { 
                var table = passangersDivs[0].childNodes[1]; 
                for (var i=0; i<table.rows.length; i++) { 
                    table.rows[i].deleteCell(1); 
                    table.rows[i].deleteCell(1); 
                }
            }
        """)
        time.sleep(2)
        elem = self.browser.find_element(By.XPATH, '//*')
        source_code = elem.get_attribute('outerHTML')
        source_code = source_code.replace('<head>', '<head><meta charset="UTF-8">')
        # https://stackoverflow.com/a/14975912/1786016
        # add styles and script for print html
        source_code = source_code.replace('</body>', '<style type="text/css" media="print">'
                                                     '@page {size: auto; margin: 0; }</style>'
                                                     '<script>window.print();</script></body>')

        self.context['results'] = {
            'source_code': source_code
        }

        self.status = WorkflowStatus.WorkflowSuccess

    @document_handler(Priority.Lowest,
                      [RuleSet(Rule('http', PlainText, CaseLess, Mandatory, Url)),
                       RuleSet(Rule('deeplink', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=10)
    def main_ajax_page(self):
        logger.info("Unhandled ajax page")
        logger.info(self.browser.current_url)
        logger.info(f'Tabs count: {len(self.browser.window_handles)}')

        if len(self.browser.window_handles) > 1:
            self.browser.close()
            self.browser.switch_to.window(self.browser.window_handles[0])
        # If current page biletix.ru then we have some error with data and must abort to not wait
        if self.browser.current_url == 'https://biletix.ru/':
            raise UserDataErrorException
        time.sleep(10)

    def __fill_passangers_data(self, index, first_name, last_name, passport_number, gender, date_of_birth, passport_date_of_expiry):
        last_name_el = self.__wait_element(xpath_selector=f'.//input[@id="PSGRDATA_PSGR_NAME_{index}"]')
        self.__click_on_element(elem=last_name_el)
        last_name_el.send_keys(last_name)
        time.sleep(1)
        first_name_el = self.__wait_element(xpath_selector=f'.//input[@id="PSGRDATA_PSGR_FNAME_{index}"]')
        self.__click_on_element(elem=first_name_el)
        first_name_el.send_keys(first_name)
        time.sleep(1)
        if gender == 'mr':
            gender_type_el = self.__wait_element(xpath_selector=f'.//div[@id="PSGRDATA_GENDER_{index}_SWITCHER"]//div[contains(@class, "gender_M")]')
        else:
            gender_type_el = self.__wait_element(xpath_selector=f'.//div[@id="PSGRDATA_GENDER_{index}_SWITCHER"]//div[contains(@class, "gender_F")]')

        self.__click_on_element(elem=gender_type_el)
        time.sleep(1)
        passport_number_el = self.__wait_element(xpath_selector=f'.//input[@id="PSGRDATA_DOCNUMBER_{index}"]')
        self.__click_on_element(elem=passport_number_el)
        passport_number_el.send_keys(passport_number)
        time.sleep(1)
        # Date of birth
        birth_date_day_el = self.__wait_element(xpath_selector=f'.//select[@id="PSGRDATA_BIRTHDATE_DAY_{index}"]')
        Select(birth_date_day_el).select_by_value(str(date_of_birth.day))

        birth_date_month_el = self.__wait_element(xpath_selector=f'.//select[@id="PSGRDATA_BIRTHDATE_MONTH_{index}"]')
        Select(birth_date_month_el).select_by_value(str(date_of_birth.month))

        birth_date_year_el = self.__wait_element(xpath_selector=f'.//select[@id="PSGRDATA_BIRTHDATE_YEAR_{index}"]')
        Select(birth_date_year_el).select_by_value(str(date_of_birth.year))
        # Passport expiry date
        passport_expiration_day_el = self.__wait_element(xpath_selector=f'.//select[@id="PSGRDATA_DOCEXPIRATION_DAY_{index}"]')
        Select(passport_expiration_day_el).select_by_value(str(passport_date_of_expiry.day))

        passport_expiration_month_el = self.__wait_element(xpath_selector=f'.//select[@id="PSGRDATA_DOCEXPIRATION_MONTH_{index}"]')
        Select(passport_expiration_month_el).select_by_value(str(passport_date_of_expiry.month))

        passport_expiration_year_el = self.__wait_element(xpath_selector=f'.//select[@id="PSGRDATA_DOCEXPIRATION_YEAR_{index}"]')
        Select(passport_expiration_year_el).select_by_value(str(passport_date_of_expiry.year))

    def __wait_element(self, xpath_selector, parent=None, timeout=15, raise_error=True):
        if not parent:
            parent = self.browser
        logger.info(f'Locating {xpath_selector}')
        try:
            WebDriverWait(self.browser, timeout).until(
                EC.visibility_of_element_located(
                    (By.XPATH, xpath_selector))
            )
        except (TimeoutException, NoSuchElementException) as e:
            if raise_error:
                logger.error(
                    f'{xpath_selector} not found. Abort.',
                    exc_info=True)
                raise e
            return None

        return parent.find_element_by_xpath(xpath_selector)

    def __click_on_element(self, elem, retries=2):
        logger.info(f'Click on element {elem}')
        try:
            elem.click()
        except Exception as e:
            if retries > 0:
                retries = retries-1
                logger.warning(f'Click failed, retry({retries})')
                time.sleep(3)
                self.__click_on_element(elem=elem, retries=retries)
            else:
                raise e


@bookingRegistry.register
class BiletixFlight(BaseBooking):
    key_name = 'Biletix'
    display_name = 'Biletix'
    main_page = 'https://biletix.ru/'
    workflow_handler = BiletixFlightWorkflowHandler
    form_template_name = 'booking/biletix_flight_form.html'

    @property
    def model_class(self):
        from ..models import BiletixFlight
        return BiletixFlight

    @property
    def form_class(self):
        from ..forms import BiletixFlightForm
        return BiletixFlightForm

    @property
    def serializer_class(self):
        from ..serializers import BiletixFlightSerializer
        return BiletixFlightSerializer

    def make_booking(self, obj):
        from ..models import Person

        profile = webdriver.FirefoxProfile()
        profile.set_preference("print.always_print_silent", True)
        profile.set_preference("print.show_print_progress", False)
        profile.set_preference("browser.link.open_newwindow", 1)
        profile.set_preference('permissions.default.image', 2)
        # https://support.mozilla.org/en-US/questions/1167673
        profile.set_preference("browser.tabs.remote.autostart", False)
        profile.set_preference("browser.tabs.remote.autostart2", False)
        profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', False)
        proxy = self.get_proxy()

        browser = webdriver.Remote(command_executor=f'http://hub:4444/wd/hub',
                                   desired_capabilities=DesiredCapabilities.FIREFOX,
                                   browser_profile=profile,
                                   proxy=proxy)
        obj.session_id = browser.session_id
        obj.save()

        try:
            browser.get(self.main_page)
        except WebDriverException as e:
            browser.quit()
            raise e

        persons = []
        gender_map = {
            Person.GENDER_MR: 'mr',
            Person.GENDER_MS: 'ms',
            Person.GENDER_MRS: 'ms',
            Person.GENDER_MISS: 'ms'
        }

        for person in obj.persons.all():
            persons.append({
                'first_name': person.first_name,
                'last_name': person.last_name,
                'gender': gender_map[person.gender],
                'date_of_birth': generate_random_date_of_birth(),
                'passport_number': generate_random_passport_number(),
                'passport_date_of_expiry': generate_random_passport_date_of_expiry(),
                'phone': generate_random_phone_number()
            })

        worker = self.workflow_handler(browser, {
            'city_from': obj.city_from,
            'city_to': obj.city_to,
            'date_from': obj.date_from,
            'date_to': obj.date_to,
            'persons': persons,
        })

        worker.dispatch_while_working()
        browser.quit()
        return worker

    def save_data(self, obj, worker_context):
        logger.info("Saving data")
        flight_ticket_file = ContentFile(worker_context['results']['source_code'])
        flight_ticket_file.name = "flight_ticket_file.html"
        obj.ticket = flight_ticket_file
        obj.save()
        logger.info("Data saved")

