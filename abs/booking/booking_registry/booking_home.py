import re
import time
import logging
import random
from datetime import datetime
from django.core.files.base import ContentFile

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException, \
    StaleElementReferenceException, WebDriverException

from .base import BaseBooking, bookingRegistry
from ..modules.workflow_handler import WorkflowHandler, WorkflowStatus, RuleSet, Rule, Priority,\
    PlainText, CaseLess, Mandatory, Url, Regex, document_handler, WorkflowError
from ..modules.exceptions import UserDataErrorException

from ..credit_card_numbers_generator import generate_card
from ..utils import generate_random_phone_number, generate_random_email, generate_random_address

logger = logging.getLogger(__name__)


class BookingHomeWorkflowHandler(WorkflowHandler):

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('www.booking.com/index.ru.html?prefer_site_type=tdot', PlainText, CaseLess, Mandatory,
                                    Url))], invocation_limit=1)
    def index_page(self):
        logger.info("Index page")
        logger.info(self.browser.current_url)

        destination_input_el = self.__wait_element(xpath_selector='//input[@id="destination"]')
        destination_input_el.send_keys(self.context['city'])
        time.sleep(10)
        autocomplete_box_el = self.__wait_element(
            xpath_selector='.//ul[contains(@class, "autocomplete")]')
        destination_li_els = autocomplete_box_el.find_elements(By.XPATH, './/li')

        if len(destination_li_els) == 0:
            workflow_error = WorkflowError(
                message=f'Город {self.context["city"]} не был найден в выпадающем списке. '
                        f'Проверьте его наличие на сайте и правильность написания')
            self.context['errors'].append(workflow_error)
            raise UserDataErrorException

        first_destination_result = destination_li_els[0]
        self.__click_on_element(elem=first_destination_result)
        logger.info("Hiding search dropdown with js")
        searchbox_div_el = self.__wait_element(xpath_selector='.//div[@id="searchbox-dropdown"]')
        self.browser.execute_script("arguments[0].style.display = 'none';", searchbox_div_el)
        time.sleep(2)
        logger.info("Close calendar")
        close_calendar_span_el = self.__wait_element(xpath_selector='.//span[@class="calendar_close"]')
        self.__click_on_element(elem=close_calendar_span_el)
        time.sleep(2)

        calendar_checkin_button = self.__wait_element('.//div[@id="homein"]//button[@type="button"]')
        self.__click_on_element(elem=calendar_checkin_button)
        time.sleep(2)
        close_calendar_span_el = self.__wait_element(xpath_selector='.//span[@class="calendar_close"]')
        self.__click_on_element(elem=close_calendar_span_el)
        time.sleep(2)

        calendar_checkout_button = self.__wait_element('.//div[@id="homeout"]//button[@type="button"]')
        self.__click_on_element(elem=calendar_checkout_button)
        time.sleep(2)
        close_calendar_span_el = self.__wait_element(xpath_selector='.//span[@class="calendar_close"]')
        self.__click_on_element(elem=close_calendar_span_el)
        time.sleep(2)



        # Select date from
        logger.info("Make visible checkin_day with js")
        checkin_day_el = self.browser.find_element(By.XPATH, './/div[@id="homein"]//select[@id = "checkin_day"]')
        self.browser.execute_script("arguments[0].parentNode.style.display = 'block';", checkin_day_el)
        self.browser.execute_script("arguments[0].style.display = 'block';", checkin_day_el)
        checkin_day_el = self.__wait_element('.//div[@id="homein"]//select[@id = "checkin_day"]')
        day = str(self.context['date_from'].day)
        logger.info(f"Select day from to {day}")
        Select(checkin_day_el).select_by_value(day)

        logger.info("Make visible checkin_year_month with js")
        checkin_year_month_el = self.browser.find_element(By.XPATH, './/div[@id="homein"]//select[@id = "checkin_year_month"]')
        self.browser.execute_script("arguments[0].style.display = 'block';", checkin_year_month_el)
        checkin_year_month_el = self.__wait_element('.//div[@id="homein"]//select[@id = "checkin_year_month"]')
        month_year = f"{self.context['date_from'].year}-{self.context['date_from'].month}"
        logger.info(f"Select year-month from to {month_year}")
        Select(checkin_year_month_el).select_by_value(month_year)

        # Select date to
        logger.info("Make visible checkout_monthday with js")
        checkout_monthday_el = self.browser.find_element(By.XPATH, './/div[@id="homeout"]//select[@id = "checkout_monthday"]')
        self.browser.execute_script("arguments[0].parentNode.style.display = 'block';", checkout_monthday_el)
        self.browser.execute_script("arguments[0].style.display = 'block';", checkout_monthday_el)
        checkout_monthday_el = self.__wait_element('.//div[@id="homeout"]//select[@id = "checkout_monthday"]')
        day = str(self.context['date_to'].day)
        logger.info(f"Select day from to {day}")
        Select(checkout_monthday_el).select_by_value(day)

        logger.info("Make visible checkout_year_month with js")
        checkout_year_month_el = self.browser.find_element(By.XPATH, './/div[@id="homeout"]//select[@id = "checkout_year_month"]')
        self.browser.execute_script("arguments[0].style.display = 'block';", checkout_year_month_el)
        checkout_year_month_el = self.__wait_element('.//div[@id="homeout"]//select[@id = "checkout_year_month"]')
        month_year = f"{self.context['date_to'].year}-{self.context['date_to'].month}"
        logger.info(f"Select year-month from to {month_year}")
        Select(checkout_year_month_el).select_by_value(month_year)

        # number of persons
        group_adults_select_el = self.browser.find_element(By.XPATH, '//select[@name = "group_adults"]')
        Select(group_adults_select_el).select_by_value(str(len(self.context['persons'])))

        submit_but_el = self.__wait_element(xpath_selector='//*[@id="frm"]//button[@class="bigbluebutton"]')
        self.__click_on_element(elem=submit_but_el)

    @document_handler(Priority.High,
                      [RuleSet(
                          Rule('booking.com/index.ru.html', PlainText, CaseLess, Mandatory,
                               Url))], invocation_limit=3)
    def index_ajax_before_results_page(self):
        logger.info("Index before ajax redirect page")
        logger.info(self.browser.current_url)
        error_el = self.__wait_element(xpath_selector='//*[@id="inout"]//*[@class="errors_dateerrors"]',
                                       timeout=5,
                                       raise_error=False)
        if error_el:
            logger.info(f'Main form has errors: {error_el.text}')
            workflow_error = WorkflowError(
                message=f'{error_el.text}')
            self.context['errors'].append(workflow_error)
            raise UserDataErrorException
        time.sleep(5)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('booking.com/searchresults.ru.html', PlainText, CaseLess, Mandatory,
                               Url))], invocation_limit=1)
    def search_results_page(self):
        logger.info("Search results page")
        logger.info(self.browser.current_url)

        self.__wait_overlay(xpath_selector='.//div[contains(@class, "sr-usp-overlay")]')
        logger.info('Set price order filter')
        price_order_el = self.__wait_element(xpath_selector='.//li[contains(@class,"sort_price")]')
        self.__click_on_element(elem=price_order_el)

        self.__wait_overlay(xpath_selector='.//div[contains(@class, "sr-usp-overlay")]')

        # logger.info('Set free booking filter')
        # price_filter_el = self.__wait_element(xpath_selector='.//a[@data-id = "fc-2"]')
        # self.__click_on_element(elem=price_filter_el)

        self.__wait_overlay(xpath_selector='//div[contains(@class, "sr-usp-overlay")]')

        logger.info('Set hotels only filter')

        hotels_only_el = self.__wait_element(xpath_selector='.//a[@data-id = "ht_id-204"]')
        self.__click_on_element(elem=hotels_only_el)

        self.__wait_overlay(xpath_selector='.//div[contains(@class, "sr-usp-overlay")]')

        try:
            select_hotel_a_el = self.__wait_element(xpath_selector='.//a[@class="super-free"]')
        except (TimeoutException, NoSuchElementException):
            logger.info('Home not found in list')
            workflow_error = WorkflowError(
                message=f'На эти даты не было найдено ни одного отеля')
            self.context['errors'].append(workflow_error)
            raise UserDataErrorException
        # Remove all target="_blank" attr from links
        self.browser.execute_script("""Array.from(document.querySelectorAll('a[target="_blank"]')).forEach(link => link.removeAttribute('target'));""")
        time.sleep(1)
        self.__click_on_element(elem=select_hotel_a_el)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('booking.com/hotel/', PlainText, CaseLess, Mandatory,
                               Url))], invocation_limit=1)
    def hotel_detail_page(self):
        logger.info("Hotel detail page")
        logger.info(self.browser.current_url)

        ul_rooms_el = self.__wait_element(xpath_selector='.//ul[contains(@class, "tdot_roomstable")]')

        try:
            recommendation_btn_el = self.__wait_element(
                xpath_selector='.//a[contains(@class, "gs-recommendation-cta")]',
                timeout=10)
            self.__click_on_element(elem=recommendation_btn_el)
        except WebDriverException:
            li_room_block_el = self.__wait_element(xpath_selector='.//li[contains(@class, "room_block")]',
                                                   parent=ul_rooms_el)
            select_room_el = self.__wait_element(xpath_selector='//select[contains(@class, "number_rooms")]',
                                                 parent=li_room_block_el)
            Select(select_room_el).select_by_value("1")

            book_btn_el = self.__wait_element(xpath_selector='//button[@name="book"]')
            self.__click_on_element(elem=book_btn_el)

    @document_handler(Priority.High,
                      [RuleSet(
                          Rule('booking.com/hotel/', PlainText, CaseLess, Mandatory,
                               Url))], invocation_limit=5)
    def hotel_detail_ajax_page(self):
        logger.info("Hotel detail ajax page")
        logger.info(self.browser.current_url)
        time.sleep(3)
        try:
            book_btn_el = self.__wait_element(xpath_selector='//button[@name="book"]')
            self.__click_on_element(elem=book_btn_el)
        except (TimeoutException, NoSuchElementException) as e:
            pass

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('secure.booking.com/book.', PlainText, CaseLess, Mandatory,
                               Url))], invocation_limit=1)
    def book_main_info_page(self):
        logger.info("Book page")
        logger.info(self.browser.current_url)
        self.browser.execute_script("window.alert = function() {};")
        self.browser.execute_script("window.onbeforeunload = function() {};")

        main_person = self.context['persons'][0]

        booker_title_el = self.__wait_element(xpath_selector='.//select[@id="booker_title"]', raise_error=False)
        if booker_title_el:
            Select(booker_title_el).select_by_value(main_person['gender'])

        first_name_input_el = self.__wait_element(xpath_selector='.//input[@id="firstname"]')
        first_name_input_el.send_keys(main_person['first_name'])

        last_name_input_el = self.__wait_element(xpath_selector='.//input[@id="lastname"]')
        last_name_input_el.send_keys(main_person['last_name'])

        email_address = generate_random_email(first_name=main_person["first_name"], last_name=main_person["last_name"])

        email_input_el = self.__wait_element(xpath_selector='//input[@id="email"]')
        email_input_el.send_keys(email_address)

        email_confirm_input_el = self.__wait_element(xpath_selector='//input[@id="email_confirm"]')
        email_confirm_input_el.send_keys(email_address)

        guest_count_select_el = self.__wait_element(xpath_selector='.//select[@data-room-guests-select=""]',
                                                    timeout=5,
                                                    raise_error=False)
        if guest_count_select_el:
            try:
                Select(guest_count_select_el).select_by_value(str(len(self.context['persons'])))
            except NoSuchElementException:
                # Ignore error when select not contain number of persons that we have
                pass
        else:
            logger.info("Guests select element not found")

        guests = list(map(lambda person: f"{person['first_name']} {person['last_name']}", self.context['persons']))
        guests = ', '.join(guests)
        guest_name_input_el = self.__wait_element(xpath_selector='.//*[contains(@class, "guest-name-input")]')
        guest_name_input_el.click()
        guest_name_input_el.clear()
        guest_name_input_el.send_keys(guests)
        time.sleep(3)
        self.set_dependency_status('BOOK_ADDITIONAL_INFO_PAGE', True)
        book_btn_el = self.__wait_element(xpath_selector='//button[@name="book"]')
        self.__click_on_element(elem=book_btn_el)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('secure.booking.com/book.', PlainText, CaseLess, Mandatory,
                               Url))], invocation_limit=3, dependencies=['BOOK_ADDITIONAL_INFO_PAGE'])
    def book_additional_info_page(self):
        logger.info("Book additional info page")
        logger.info(self.browser.current_url)

        self.browser.execute_script("window.alert = function() {};")
        self.browser.execute_script("window.onbeforeunload = function() {};")

        self.set_dependency_status('BOOK_ADDITIONAL_INFO_PAGE', False)
        self.set_dependency_status('BOOK_AJAX_WAIT_PAGE', True)

        main_person = self.context['persons'][0]

        country_select_el = self.__wait_element(xpath_selector='//select[@id="cc1"]')
        Select(country_select_el).select_by_value("ru")

        try:
            address = generate_random_address()
            address1_input_el = self.__wait_element(xpath_selector='//input[@id="address1"]', timeout=10)
            address1_input_el.click()
            address1_input_el.clear()
            time.sleep(1)
            address1_input_el.send_keys(address)
        except (TimeoutException, NoSuchElementException) as e:
            logger.info("Address field not found")
            pass

        try:
            city_input_el = self.__wait_element(xpath_selector='//input[@id="city"]', timeout=10)
            city_input_el.click()
            city_input_el.clear()
            time.sleep(1)
            city_input_el.send_keys('Москва')
            time.sleep(1)
        except (TimeoutException, NoSuchElementException) as e:
            logger.info("City field not found")
            pass

        phone_country_select_el = self.__wait_element(
            xpath_selector='//select[contains(@class, "c-input-phone-country__country")]')
        Select(phone_country_select_el).select_by_value("RU")
        time.sleep(1)
        phone = generate_random_phone_number()
        formatted_phone = phone.replace('+7', '')
        phone_input_el = self.__wait_element(xpath_selector='//input[@id="phone"]')
        phone_input_el.click()
        phone_input_el.clear()
        time.sleep(1)
        phone_input_el.send_keys(formatted_phone)
        time.sleep(1)

        cc_type_select_el = self.__wait_element(xpath_selector='//select[@id="cc_type"]', raise_error=False)
        if cc_type_select_el:
            Select(cc_type_select_el).select_by_value("Visa")

        credit_card_number, cvv = generate_card(card_type='visa16')

        cc_number_input_el = self.__wait_element(xpath_selector='//input[@id="cc_number"]')
        cc_number_input_el.click()
        cc_number_input_el.clear()
        time.sleep(1)
        credit_card_number_parts = [credit_card_number[i:i + 4] for i in range(0, len(credit_card_number), 4)]

        for number_part in credit_card_number_parts:
            cc_number_input_el.send_keys(number_part)
            time.sleep(1)

        expire_cc_month = f'{random.randint(1, 12):02}'
        expire_cc_year = f'{random.randint(2022, 2026)}'

        cc_month_select_el = self.__wait_element(xpath_selector='//select[@id="cc_month"]')
        Select(cc_month_select_el).select_by_value(expire_cc_month)

        cc_year_select_el = self.__wait_element(xpath_selector='//select[@id="ccYear"]')
        Select(cc_year_select_el).select_by_value(expire_cc_year)

        try:
            cc_cvc_input_el = self.__wait_element(xpath_selector='//input[@id="cc_cvc"]', timeout=10)
            cc_cvc_input_el.click()
            cc_cvc_input_el.clear()
            time.sleep(1)
            cc_cvc_input_el.send_keys(cvv)
            time.sleep(1)
        except (TimeoutException, NoSuchElementException):
            pass

        try:
            mailing_list_input_el = self.__wait_element(xpath_selector='//input[@id="mailinglist"]')
            self.__click_on_element(mailing_list_input_el)
        except (TimeoutException, NoSuchElementException):
            pass

        time.sleep(3)

        book_btn_el = self.__wait_element(
            xpath_selector='//button[@name="book" and contains(@class, "bp-overview-buttons-submit")]')
        self.__click_on_element(elem=book_btn_el)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('secure.booking.com/book.html', PlainText, CaseLess, Mandatory,
                               Url))], invocation_limit=3, dependencies=['BOOK_AJAX_WAIT_PAGE'])
    def booking_ajax_wait_page(self):
        logger.info("Book ajax wait page")
        logger.info(self.browser.current_url)
        logger.info("Locating errors on page")
        time.sleep(5)
        try:
            error_el = self.__wait_element(xpath_selector='.//div[@class="top-warning-wrap accessible-warning"]',
                                           timeout=5)
            error_el.click()
            logger.info("Found user errors on page. Fill form again.")
            self.set_dependency_status('BOOK_ADDITIONAL_INFO_PAGE', True)
            self.set_dependency_status('BOOK_AJAX_WAIT_PAGE', False)
        except WebDriverException:
            logger.info("Errors not found, waiting...")

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('secure.booking.com/confirmation.ru.html', PlainText, CaseLess, Mandatory,
                               Url))], invocation_limit=1)
    def booking_confirmation_page(self):
        logger.info("Book confirmation page")
        logger.info(self.browser.current_url)

        try:
            close_button_modal = self.__wait_element(xpath_selector='.//button[@class="modal-mask-closeBtn"]')
            logger.info("Login modal exist, close it")
            self.__click_on_element(elem=close_button_modal)
        except (TimeoutException, NoSuchElementException) as e:
            pass

        self.context['results'] = {}
        self.context['results']['booking_confirmation_url'] = self.browser.current_url

        elems_to_hide_for_print = self.browser.find_elements(By.XPATH, './/*[contains(@class, "hide_this_for_print")]')
        for elem in elems_to_hide_for_print:
            self.browser.execute_script('arguments[0].style.display = "none";', elem)

        elems_to_show_for_print = self.browser.find_elements(By.XPATH, './/*[contains(@class, "show_for_print")]')
        for elem in elems_to_show_for_print:
            self.browser.execute_script('arguments[0].style.display = "block";', elem)

        elem = self.browser.find_element(By.XPATH, '//*')
        source_code = elem.get_attribute('outerHTML')
        # https://stackoverflow.com/a/14975912/1786016
        # add styles and script for print html
        source_code = source_code.replace('</body>', '<style type="text/css" media="print">'
                                                     '@page {size: auto; margin: 0; }</style>'
                                                     '<script>window.print();</script></body>')

        self.context['results']['source_code'] = source_code

        self.status = WorkflowStatus.WorkflowSuccess

    def __wait_overlay(self, xpath_selector):
        logger.info(f'Waiting overlay {xpath_selector}')
        try:
            WebDriverWait(self.browser, 5).until(
                EC.visibility_of_element_located(
                    (By.XPATH, xpath_selector))
            )
            overlay_el = self.browser.find_element_by_xpath(xpath_selector)
            while overlay_el.is_displayed():
                time.sleep(0.2)
        except (StaleElementReferenceException, TimeoutException, NoSuchElementException):
            pass

    def __wait_element(self, xpath_selector, parent=None, timeout=15, raise_error=True):
        if not parent:
            parent = self.browser
        logger.info(f'Locating {xpath_selector}')
        try:
            WebDriverWait(self.browser, timeout).until(
                EC.visibility_of_element_located(
                    (By.XPATH, xpath_selector))
            )
        except (TimeoutException, NoSuchElementException) as e:
            if raise_error:
                logger.error(
                    f'{xpath_selector} not found. Abort.',
                    exc_info=True)
                raise e
            return None

        return parent.find_element_by_xpath(xpath_selector)

    def __click_on_element(self, elem, retries=2):
        logger.info(f'Click on element {elem}')
        try:
            elem.click()
        except WebDriverException as e:
            if retries > 0:
                retries = retries-1
                logger.warning(f'Click failed, retry({retries})')
                time.sleep(3)
                self.__click_on_element(elem=elem, retries=retries)
            else:
                raise e


@bookingRegistry.register
class BookingHome(BaseBooking):
    key_name = 'Booking'
    display_name = 'Booking'
    main_page = 'https://www.booking.com/index.ru.html?prefer_site_type=tdot&selected_currency=RUB'
    workflow_handler = BookingHomeWorkflowHandler
    form_template_name = 'booking/booking_home_form.html'

    @property
    def model_class(self):
        from ..models import BookingHome
        return BookingHome

    @property
    def form_class(self):
        from ..forms import BookingHomeForm
        return BookingHomeForm

    @property
    def serializer_class(self):
        from ..serializers import BookingHomeSerializer
        return BookingHomeSerializer

    def make_booking(self, obj):
        from ..models import Person

        profile = webdriver.FirefoxProfile()
        profile.set_preference("print.always_print_silent", True)
        profile.set_preference("print.show_print_progress", False)
        profile.set_preference("browser.link.open_newwindow", 1)
        # https://support.mozilla.org/en-US/questions/1167673
        profile.set_preference("browser.tabs.remote.autostart", False)
        profile.set_preference("browser.tabs.remote.autostart2", False)
        profile.set_preference('permissions.default.image', 2)
        profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', False)

        proxy = self.get_proxy()
        browser = webdriver.Remote(command_executor='http://hub:4444/wd/hub',
                                   desired_capabilities=DesiredCapabilities.FIREFOX,
                                   browser_profile=profile,
                                   proxy=proxy)
        obj.session_id = browser.session_id
        obj.save()

        try:
            browser.get(self.main_page)
        except WebDriverException as e:
            browser.quit()
            raise e
        time.sleep(15)
        persons = []
        gender_map = {
            Person.GENDER_MR: 'mr',
            Person.GENDER_MS: 'ms',
            Person.GENDER_MRS: 'ms',
            Person.GENDER_MISS: 'ms'
        }

        for person in obj.persons.all():
            persons.append({
                'first_name': person.first_name,
                'last_name': person.last_name,
                'gender': gender_map[person.gender],
            })

        worker = self.workflow_handler(browser, {
            'city': obj.city,
            'date_from': obj.date_from,
            'date_to': obj.date_to,
            'persons': persons
        })

        worker.dispatch_while_working()
        browser.quit()
        return worker

    def save_data(self, obj, worker_context):
        logger.info("Saving data")
        booking_confirmation_file = ContentFile(worker_context['results']['source_code'])
        booking_confirmation_file.name = "booking_confirmation_file.html"
        obj.ticket = booking_confirmation_file
        obj.booking_confirmation_url = worker_context['results']['booking_confirmation_url']
        obj.save()
        logger.info("Data saved")
