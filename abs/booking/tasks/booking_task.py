import base64
import traceback
import time
import requests
from functools import wraps

from abs.taskapp.celery import app
from celery.utils.log import get_task_logger
from celery.five import monotonic

from django.core.files.base import ContentFile
from django.contrib.contenttypes.models import ContentType
from django.conf import settings
from django.db.models import Q
from django.core.cache import cache

from selenium.common.exceptions import WebDriverException

from ..models import TaskError
from ..booking_registry import bookingRegistry
from ..modules.workflow_handler import WorkflowStatus, WorkflowError
from ..modules.exceptions import ProxyNotFoundException

logger = get_task_logger(__name__)


def single_instance_task(timeout):
    """
    Requires redis cache
    :param timeout: When cache will be deleted
    :return: task
    """
    def task_exc(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            lock_id = "celery-single-instance-" + func.__name__
            acquire_lock = lambda: cache.add(lock_id, "true", timeout)
            release_lock = lambda: cache.delete(lock_id)
            if acquire_lock():
                try:
                    func(*args, **kwargs)
                finally:
                    release_lock()
        return wrapper
    return task_exc


@app.task(bind=True, name='restart_booking_tasks')
def restart_booking_tasks(task):
    """
    Restart all In Queue and In Progress tasks. Usefully after server update
    :param task:
    :return:
    """
    in_queue_bookings = {}

    for key in bookingRegistry.keys():
        booking_registry_obj = bookingRegistry[key]()
        model = booking_registry_obj.model_class
        model.objects.filter(status=model.TASK_IN_PROGRESS_STATUS).update(status=model.TASK_IN_QUEUE_STATUS)
        in_queue_bookings[key] = model.objects.filter(status=model.TASK_IN_QUEUE_STATUS)

    for key, in_queue_queryset in in_queue_bookings.items():
        for obj in in_queue_queryset:
            make_booking_task.delay(obj_pk=obj.pk, key_name=key)


@app.task(bind=True, name='booking_tasks_monitor')
@single_instance_task(60*10)
def booking_tasks_monitor(task):
    resp = requests.get(url='http://hub:4444/grid/api/hub')
    data = resp.json()

    free = data['slotCounts']['free']
    total = data['slotCounts']['total']
    booking_objs = []
    in_progress_objs_count = 0

    for key in bookingRegistry:
        booking_registry_obj = bookingRegistry[key]()
        model = booking_registry_obj.model_class
        in_progress_objs_count += model.objects.filter(status=model.TASK_IN_PROGRESS_STATUS).count()
        objs = model.objects.filter(status=model.TASK_IN_QUEUE_STATUS).order_by('created_at')
        booking_objs += map(lambda obj: (key, obj), list(objs))

    if in_progress_objs_count == total-free:
        booking_objs.sort(key=lambda obj: obj[1].created_at)
        booking_objs = booking_objs[:free]
        for key, obj in booking_objs:
            obj.status = obj.TASK_IN_PROGRESS_STATUS
            obj.save()
            make_booking_task.delay(obj_pk=obj.pk, key_name=key)


@app.task(bind=True, name='make_booking_task')
def make_booking_task(task, obj_pk, key_name, retries=1):
    booking_registry_obj = bookingRegistry[key_name]()
    model = booking_registry_obj.model_class
    try:
        obj = model.objects.get(pk=obj_pk)
    except model.DoesNotExist:
        return
    logger.info('Start booking worker')
    try:
        booking_worker = booking_registry_obj.make_booking(obj=obj)
    except WebDriverException:
        logger.warning('Retry task', exc_info=True)
        if retries > 0:
            retries = retries-1
            logger.error('Error found. Retry', exc_info=True)
            task_error = TaskError(
                message='Error',
                exec_info=traceback.format_exc(),
                content_type=ContentType.objects.get_for_model(obj),
                object_id=obj.pk
            )
            task_error.save()
            time.sleep(5)
            make_booking_task.delay(obj_pk=obj_pk,
                                    key_name=key_name,
                                    retries=retries)
        else:
            logger.error('Out if retries', exc_info=True)
            obj.status = obj.TASK_FAILED_STATUS
            obj.save()
        return
    except ProxyNotFoundException:
        task_error = TaskError(
            message='Working proxy not found',
            content_type=ContentType.objects.get_for_model(obj),
            object_id=obj.pk,
        )
        task_error.save()
        obj.status = obj.TASK_FAILED_STATUS
        obj.save()
        return
    except Exception:
        logger.error('Unhandled exception for make booking', exc_info=True)
        task_error = TaskError(
            message='Unhandled exception for make booking',
            exec_info=traceback.format_exc(),
            content_type=ContentType.objects.get_for_model(obj),
            object_id=obj.pk,
        )
        task_error.save()
        obj.status = obj.TASK_FAILED_STATUS
        obj.save()
        return

    logger.info(f'Booking worker finished with status {booking_worker.status}')

    if booking_worker.status == WorkflowStatus.WorkflowSuccess:
        booking_registry_obj.save_data(obj=obj, worker_context=booking_worker.context)
        obj.status = obj.TASK_SUCCESS_STATUS
    else:
        obj.status = obj.TASK_FAILED_STATUS
        workflow_errors = booking_worker.context.get('errors', [])

        for workflow_error in workflow_errors:

            task_error = TaskError(
                message=workflow_error.message,
                exec_info=workflow_error.exec_info,
                content_type=ContentType.objects.get_for_model(obj),
                object_id=obj.pk,
            )

            if workflow_error.screenshot:
                data = ContentFile(base64.b64decode(workflow_error.screenshot), name='screenshot_error.png')
                task_error.screenshot = data

            if workflow_error.page_source:
                task_error.page_source = ContentFile(workflow_error.page_source, name='page_source.html')

            task_error.save()
    obj.save()
