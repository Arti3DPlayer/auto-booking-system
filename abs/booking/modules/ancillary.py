import re


def regex_test(pattern, source):
    m = re.match(pattern, source, re.M|re.I|re.S)

    if m:
        return True
    else:
        return False


def regex_match(pattern, source, index=None):
    m = re.match( pattern, source, re.M|re.I|re.S)

    if m:
        if index is None:
            return list(m.groups())

        return m.group(index)
    else:
        return u""


def regex_search(pattern, source, index=None):
    m = re.search(pattern, source, re.M|re.I|re.S)

    if m:
        if index:
            return m.group(index)
        return list(m.groups())

    return u""
