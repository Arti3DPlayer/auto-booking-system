

class UserDataErrorException(Exception):
    """
    Raises when user data(first_name, last_name etc.) is not correct
    """
    pass


class ProxyNotFoundException(Exception):
    """
       Raises when working proxy not been found
    """
    pass
