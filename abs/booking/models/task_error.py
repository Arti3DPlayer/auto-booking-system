from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class TaskError(models.Model):
    message = models.TextField(blank=True)
    exec_info = models.TextField(blank=True)
    screenshot = models.FileField(upload_to='task_error_screenshots', null=True, blank=True)
    page_source = models.FileField(blank=True, null=True, upload_to='task_error_page_sources')

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    created_at = models.DateTimeField(auto_now_add=True, editable=False)
