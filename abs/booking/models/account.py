from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from ..booking_registry import bookingRegistry


class BookingAccount(models.Model):
    username = models.CharField(max_length=50, blank=True)
    email = models.EmailField()
    password = models.CharField(max_length=50, blank=True)
    is_active = models.BooleanField(default=True)

    booking_site = models.CharField(max_length=50, choices=bookingRegistry.as_choices(), blank=True)

    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    def __str__(self):
        return f'Account {self.email} {self.content_type}'
