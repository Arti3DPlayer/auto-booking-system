from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class Person(models.Model):
    GENDER_MR = 1
    GENDER_MS = 2
    GENDER_MRS = 3
    GENDER_MISS = 4
    GENDER_CHOICES = (
        (GENDER_MR, 'mr'),
        (GENDER_MS, 'ms'),
        (GENDER_MRS, 'mrs'),
        (GENDER_MISS, 'miss')
    )
    first_name = models.CharField(max_length=255, help_text='Только латиницей')
    last_name = models.CharField(max_length=255, help_text='Только латиницей')
    date_of_birth = models.DateField(null=True, blank=True)
    gender = models.IntegerField(choices=GENDER_CHOICES)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    def get_html_info(self):
        return f'{self.first_name} {self.last_name}'

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

