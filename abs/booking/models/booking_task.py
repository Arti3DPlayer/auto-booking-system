from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils.translation import ugettext_lazy as _


class BookingTask(models.Model):
    user = models.ForeignKey('users.User', verbose_name=_('User'), help_text=_('Task owner'), on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
