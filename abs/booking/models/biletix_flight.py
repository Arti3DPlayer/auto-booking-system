from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.utils.translation import ugettext_lazy as _

from .booking_base import BookingBase


class BiletixFlight(BookingBase):
    city_from = models.CharField(max_length=255)
    city_to = models.CharField(max_length=255)
    date_from = models.DateField()
    date_to = models.DateField()
    ticket = models.FileField(upload_to='biletix_flight', null=True, blank=True)

    def get_html_info(self):
        return f'<b>Biletix</b><br>' \
               f'{self.city_from} -> {self.city_to}<br>' \
               f'{self.date_from} - {self.date_to}<br>' \
               f'{"<br>".join([person.get_html_info() for person in self.persons.all()])}'

    def get_results_html_info(self):
        if self.ticket:
            return f'<a href="{self.ticket.url}" target="_blank">Flight ticket</a>'
        return ''
