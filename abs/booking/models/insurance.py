from django.db import models
from django.utils import timezone
from django.contrib.contenttypes.fields import GenericRelation
from django.utils.translation import ugettext_lazy as _


class Insurance(models.Model):
    ZETTA_COMPANY = 0

    COMPANY_CHOICES = (
        (ZETTA_COMPANY, _(u'Zetta')),
    )
    booking_task = models.ForeignKey('booking.BookingTask', null=True, blank=True, on_delete=models.CASCADE)
    company = models.PositiveSmallIntegerField(choices=COMPANY_CHOICES, default=ZETTA_COMPANY)
    policyholder = models.CharField(max_length=255)
    issue_date = models.DateField(default=timezone.now)
    date_from = models.DateField()
    date_to = models.DateField()
    persons = GenericRelation('booking.Person')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    def __str__(self):
        return f'Insurance ({self.get_company_display()})'
