from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.utils.translation import ugettext_lazy as _


class BookingBase(models.Model):
    TASK_IN_QUEUE_STATUS = 0
    TASK_IN_PROGRESS_STATUS = 1
    TASK_FAILED_STATUS = 2
    TASK_SUCCESS_STATUS = 3

    STATUS_CHOICES = (
        (TASK_IN_QUEUE_STATUS, _(u'In queue')),
        (TASK_IN_PROGRESS_STATUS, _(u'In progress')),
        (TASK_FAILED_STATUS, _(u'Failed')),
        (TASK_SUCCESS_STATUS, _(u'Success')),
    )
    booking_task = models.ForeignKey('booking.BookingTask', on_delete=models.CASCADE)
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES,
                                              default=TASK_IN_QUEUE_STATUS)
    session_id = models.CharField(max_length=255, blank=True)
    persons = GenericRelation('booking.Person')
    task_errors = GenericRelation('booking.TaskError')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        abstract = True
