from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.utils.translation import ugettext_lazy as _

from .booking_base import BookingBase


class OstrovokHome(BookingBase):
    city = models.CharField(max_length=255)
    date_from = models.DateField()
    date_to = models.DateField()
    booking_print_url = models.TextField(blank=True)
    booking_confirmation_url = models.TextField(blank=True)

    def get_html_info(self):
        return f'<b>Ostrovok.ru</b><br>' \
               f'{self.city}<br>' \
               f'{self.date_from} - {self.date_to}<br>' \
               f'{"<br>".join([person.get_html_info() for person in self.persons.all()])}'

    def get_results_html_info(self):
        if self.booking_print_url and self.booking_confirmation_url:
            return f'<a href="{self.booking_print_url}" target="_blank">Confirmation pdf</a><br>' \
                   f'<a href="{self.booking_confirmation_url}" target="_blank">Confirmation link</a>'
        return ''
