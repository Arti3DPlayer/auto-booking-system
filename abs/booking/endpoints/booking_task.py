from __future__ import print_function, division, absolute_import, unicode_literals

from django.db import transaction
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from ..tasks import make_booking_task
from ..booking_registry import bookingRegistry
from ..serializers import BookingTaskSerializer, InsuranceSerializer


class BookingTaskEndpoint(APIView):

    def post(self, request, *args, **kwargs):
        booking_task_serializer = BookingTaskSerializer(data=request.data.get('booking_task'),
                                                        context={'request': request})
        booking_registry_data = request.data.get('booking_registry')
        insurances_data = request.data.get('insurances')

        if not type(booking_registry_data) is list:
            return Response({'errors': 'booking_registry_data[] is required'}, status=status.HTTP_400_BAD_REQUEST)
        if not type(insurances_data) is list:
            return Response({'errors': 'insurances_data[] is required'}, status=status.HTTP_400_BAD_REQUEST)
        if booking_task_serializer.is_valid():
            with transaction.atomic():
                sid = transaction.savepoint()
                booking_task = booking_task_serializer.save()
                # booking_objs = []
                for data in booking_registry_data:
                    key_name = data['key_name']
                    data['booking_task'] = booking_task.pk
                    booking_registry_serializer = bookingRegistry[key_name]().serializer_class(data=data)
                    if booking_registry_serializer.is_valid():
                        obj = booking_registry_serializer.save()
                        # booking_objs.append({'obj_pk': obj.pk, 'key': key_name})
                    else:
                        transaction.savepoint_rollback(sid)
                        return Response(booking_registry_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                for data in insurances_data:
                    data['booking_task'] = booking_task.pk
                    insurance_serializer = InsuranceSerializer(data=data)
                    if insurance_serializer.is_valid():
                        insurance_serializer.save()
                    else:
                        transaction.savepoint_rollback(sid)
                        return Response(insurance_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            # for booking_obj in booking_objs:
            #     make_booking_task.delay(obj_pk=booking_obj['obj_pk'], key_name=booking_obj['key'])
            return Response(booking_task_serializer.data, status=status.HTTP_201_CREATED)

        return Response(booking_task_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
