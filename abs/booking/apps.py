from django.apps import AppConfig


class BookingConfig(AppConfig):
    name = 'abs.booking'
