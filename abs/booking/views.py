from django.contrib import messages
from django.http import HttpResponseRedirect
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import TemplateView, CreateView, FormView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin


from .booking_registry import bookingRegistry
from .models import TaskError, Insurance
from .tasks import restart_booking_tasks
from .forms import PersonForm, InsuranceForm


@method_decorator(login_required, name='dispatch')
class BookingTasksView(TemplateView):
    template_name = 'booking/booking_tasks.html'


class BookingTasksTableView(LoginRequiredMixin, TemplateView):
    template_name = 'booking/booking_tasks_table.html'

    def get_context_data(self, **kwargs):
        ctx = super(BookingTasksTableView, self).get_context_data(**kwargs)
        ctx['booking_objects'] = []
        for booking_registry_key in bookingRegistry.keys():
            b_obj = bookingRegistry[booking_registry_key]()
            if self.request.user.is_superuser:
                booking_objects = b_obj.model_class.objects.all()
            else:
                booking_objects = b_obj.model_class.objects.filter(booking_task__user=self.request.user)
            ctx['booking_objects'] += list(booking_objects)

        if self.request.user.is_superuser:
            insurances = Insurance.objects.all()
        else:
            insurances = Insurance.objects.filter(booking_task__user=self.request.user)

        ctx['booking_objects'] += list(insurances)

        ctx['booking_objects'].sort(key=lambda obj: obj.created_at, reverse=True)

        page = self.request.GET.get('page', 1)
        paginator = Paginator(ctx['booking_objects'], 20)

        try:
            ctx['booking_objects'] = paginator.page(page)
        except PageNotAnInteger:
            ctx['booking_objects'] = paginator.page(1)
        except EmptyPage:
            ctx['booking_objects'] = paginator.page(paginator.num_pages)

        return ctx


@method_decorator(login_required, name='dispatch')
class CreateBookingTaskView(TemplateView):
    template_name = 'booking/change_booking_task.html'


@method_decorator(login_required, name='dispatch')
class GetPersonFormView(FormView):
    form_class = PersonForm
    template_name = 'booking/person_form.html'


@method_decorator(login_required, name='dispatch')
class GetInsuranceFormView(FormView):
    form_class = InsuranceForm
    template_name = 'booking/insurance_form.html'


class RenderInsuranceTemplate(DetailView):
    model = Insurance
    template_name = 'booking/insurance.html'

    def get_context_data(self, **kwargs):
        ctx = super(RenderInsuranceTemplate, self).get_context_data(**kwargs)
        delta = self.object.date_to - self.object.date_from
        ctx['date_delta_days'] = delta.days + 1  # include date_from day
        ctx['persons'] = self.object.persons.all().order_by('created_at')
        return ctx


@method_decorator(login_required, name='dispatch')
class GetBookingFormView(FormView):

    def get_form_class(self):
        key_name = self.request.GET.get('key_name')
        booking_obj = bookingRegistry[key_name]()
        self.form_class = booking_obj.form_class
        return self.form_class

    def get_template_names(self):
        key_name = self.request.GET.get('key_name')
        booking_obj = bookingRegistry[key_name]()
        self.template_name = booking_obj.form_template_name
        return self.template_name


@method_decorator(login_required, name='dispatch')
class BookingObjectTaskErrorDetailView(DetailView):
    model = TaskError
    template_name = 'booking/task_error_detail.html'

    def get_object(self, queryset=None):
        ct_pk = self.kwargs.get('ct')
        obj_pk = self.kwargs.get('pk')
        ct = get_object_or_404(ContentType, pk=ct_pk)
        return get_object_or_404(ct.model_class(), pk=obj_pk)


def restart_booking_tasks_view(request):
    restart_booking_tasks.delay()
    messages.add_message(request, messages.INFO, 'Task for monitoring started')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
