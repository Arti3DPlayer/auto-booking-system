from rest_framework import serializers

from ..models import Person


class PersonSerializer(serializers.ModelSerializer):
    date_of_birth = serializers.DateField(input_formats=['%m/%d/%Y', ])

    class Meta:
        model = Person
        exclude = ('content_type', 'object_id')
