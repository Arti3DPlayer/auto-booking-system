from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _
from ..models import S7Flight

from .booking_base import BookingBaseSerializer


class S7FlightSerializer(BookingBaseSerializer):
    date_from = serializers.DateField(input_formats=['%m/%d/%Y', ])
    date_to = serializers.DateField(input_formats=['%m/%d/%Y', ])

    class Meta:
        model = S7Flight
        exclude = ('ticket',)
