from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType

from ..models import Person

from .person import PersonSerializer


class BookingBaseSerializer(serializers.ModelSerializer):
    persons = PersonSerializer(many=True)

    def validate_persons(self, persons):
        if len(persons) == 0:
            raise serializers.ValidationError(_('At least one person required'))
        return persons

    def create(self, validated_data):
        persons_data = validated_data.pop('persons')
        model = self.Meta.model
        obj = model.objects.create(**validated_data)
        for person_data in persons_data:
            Person.objects.create(content_type=ContentType.objects.get_for_model(model),
                                  object_id=obj.pk, **person_data)
        return obj
