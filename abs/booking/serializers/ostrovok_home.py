from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _
from ..models import OstrovokHome

from .booking_base import BookingBaseSerializer


class OstrovokHomeSerializer(BookingBaseSerializer):
    date_from = serializers.DateField(input_formats=['%m/%d/%Y', ])
    date_to = serializers.DateField(input_formats=['%m/%d/%Y', ])

    class Meta:
        model = OstrovokHome
        exclude = ('booking_print_url', 'booking_confirmation_url')
