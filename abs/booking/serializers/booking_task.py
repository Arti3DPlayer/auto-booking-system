from rest_framework import serializers

from ..models import BookingTask


class BookingTaskSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = BookingTask
        fields = '__all__'
