from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType
from django.core.files.base import ContentFile
from django.template.loader import render_to_string

from ..models import Insurance, Person

from .person import PersonSerializer


class InsuranceSerializer(serializers.ModelSerializer):
    date_from = serializers.DateField(input_formats=['%m/%d/%Y', ])
    date_to = serializers.DateField(input_formats=['%m/%d/%Y', ])
    persons = PersonSerializer(many=True)

    def validate_persons(self, persons):
        if len(persons) == 0:
            raise serializers.ValidationError(_('At least one person required'))
        return persons

    def create(self, validated_data):
        persons_data = validated_data.pop('persons')
        persons = []
        model = self.Meta.model
        obj = model.objects.create(**validated_data)
        for person_data in persons_data:
            persons.append(Person.objects.create(content_type=ContentType.objects.get_for_model(model),
                                                 object_id=obj.pk, **person_data))
        return obj

    class Meta:
        model = Insurance
        fields = '__all__'
