from .booking_task import BookingTaskSerializer
from .klm_flight import KLMFlightSerializer
from .biletix_flight import BiletixFlightSerializer
from .booking_home import BookingHomeSerializer
from .ostrovok_home import OstrovokHomeSerializer
from .s7_flight import S7FlightSerializer
from .insurance import InsuranceSerializer
