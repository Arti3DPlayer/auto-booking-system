from django.contrib import admin

from ..models import OstrovokHome

from .person import PersonGenericTabularInline
from .task_error import TaskErrorGenericTabularInline


@admin.register(OstrovokHome)
class OstrovokHomeAdmin(admin.ModelAdmin):
    list_display = ('pk', 'status')
    inlines = (PersonGenericTabularInline, TaskErrorGenericTabularInline)


class OstrovokHomeInline(admin.TabularInline):
    model = OstrovokHome
    extra = 0
