from django.contrib import admin

from ..models import KLMFlight

from .person import PersonGenericTabularInline
from .task_error import TaskErrorGenericTabularInline


@admin.register(KLMFlight)
class KLMFlightAdmin(admin.ModelAdmin):
    list_display = ('pk', 'status')
    inlines = (PersonGenericTabularInline, TaskErrorGenericTabularInline)


class KLMFlightInline(admin.TabularInline):
    model = KLMFlight
    extra = 0
