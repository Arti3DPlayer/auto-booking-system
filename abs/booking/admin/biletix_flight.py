from django.contrib import admin

from ..models import BiletixFlight

from .person import PersonGenericTabularInline
from .task_error import TaskErrorGenericTabularInline


@admin.register(BiletixFlight)
class BiletixFlightAdmin(admin.ModelAdmin):
    list_display = ('pk', 'status')
    inlines = (PersonGenericTabularInline, TaskErrorGenericTabularInline)


class BiletixFlightInline(admin.TabularInline):
    model = BiletixFlight
    extra = 0
