from django.contrib import admin

from ..models import Insurance

from .person import PersonGenericTabularInline


@admin.register(Insurance)
class InsuranceAdmin(admin.ModelAdmin):
    list_display = ('pk', 'company')
    inlines = (PersonGenericTabularInline, )
