from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

from ..models import TaskError


class TaskErrorGenericTabularInline(GenericTabularInline):
    model = TaskError
    extra = 0

    def has_add_permission(self, request):
        return False
