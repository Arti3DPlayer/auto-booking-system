from django.contrib import admin

from ..models import S7Flight

from .person import PersonGenericTabularInline
from .task_error import TaskErrorGenericTabularInline


@admin.register(S7Flight)
class S7FlightAdmin(admin.ModelAdmin):
    list_display = ('pk', 'status')
    inlines = (PersonGenericTabularInline, TaskErrorGenericTabularInline)


class S7FlightInline(admin.TabularInline):
    model = S7Flight
    extra = 0
