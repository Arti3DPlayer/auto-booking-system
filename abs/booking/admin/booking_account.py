from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

from ..models import BookingAccount


@admin.register(BookingAccount)
class BookingAccountAdmin(admin.ModelAdmin):
    list_display = ('pk', )


class BookingAccountGenericTabularInline(GenericTabularInline):
    model = BookingAccount
    extra = 0
