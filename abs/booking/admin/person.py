from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

from ..models import Person


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('pk', )


class PersonGenericTabularInline(GenericTabularInline):
    model = Person
    extra = 0
