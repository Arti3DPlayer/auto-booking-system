from django.contrib import admin

from ..models import BookingHome

from .person import PersonGenericTabularInline
from .task_error import TaskErrorGenericTabularInline


@admin.register(BookingHome)
class BookingHomeAdmin(admin.ModelAdmin):
    list_display = ('pk', 'status')
    inlines = (PersonGenericTabularInline, TaskErrorGenericTabularInline)


class BookingHomeInline(admin.TabularInline):
    model = BookingHome
    extra = 0
