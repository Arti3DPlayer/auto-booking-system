from django.urls import path
from django.contrib import admin

from ..models import BookingTask
from ..views import restart_booking_tasks_view

from .klm_flight import KLMFlightInline
from .booking_home import BookingHomeInline


@admin.register(BookingTask)
class BookingTaskAdmin(admin.ModelAdmin):
    change_list_template = 'admin/booking/booking_task/change_list.html'
    list_display = ('pk', 'user', 'created_at')
    inlines = (KLMFlightInline, BookingHomeInline)

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('restart_booking_tasks/',
                 self.admin_site.admin_view(restart_booking_tasks_view), name='restart_booking_tasks_view'),
        ]
        return my_urls + urls






