function BookingTasksPage() {
    this.currentPage = 1;
    this.userAction = false;
    this.updateInterval = 5000;
}

BookingTasksPage.prototype.init = function () {
    let self = this;
    self.fetchTasks();
    this.updateTableTimeout = setTimeout(function () {
        self.fetchTasksInterval();
    }, self.updateInterval);

    $("#booking-tasks-container").on('click', '.collapser', function() {
        $(this).next().collapse('toggle');
    });

    $("#booking-tasks-container").on('click', '.host-pagination .page-link', function (e) {
        e.preventDefault();
        self.currentPage = $(this).attr('data-page');
        self.fetchTasks();
    });
};

BookingTasksPage.prototype.fetchTasksInterval = function () {
    let self = this;
    self.updateTableTimeout = setTimeout(function () {
        self.fetchTasksInterval();
    }, self.updateInterval);
    if (!self.userAction) {
        self.fetchTasks();
    } else {
        self.userAction = false;
    }
};

BookingTasksPage.prototype.fetchTasks = function () {
    $("#booking-tasks-container").load('/booking/tasks/table/?page='+this.currentPage);
};

let bookingTasksPage = new BookingTasksPage();
bookingTasksPage.init();
