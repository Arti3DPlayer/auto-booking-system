function ChangeBookingTaskPage() {
}

ChangeBookingTaskPage.prototype.init = function () {
    let self = this;

    $('#addPersonButton').click(function (e) {
        e.preventDefault();
        self.addPersonForm();
    });

    $("#personForms").on('click', '.card .card-header .close', function (e) {
        e.preventDefault();
        let index = $(this).attr('data-index');
        if (index) {
            self.removePersonForm(index);
        }
    });

    $("#chainForms").on('click', '.card .card-header .close', function (e) {
        e.preventDefault();
        let index = $(this).attr('data-index');
        if (index) {
            self.removeChainForm(index);
        }
    });

    $("#insuranceForms").on('click', '.card .card-header .close', function (e) {
        e.preventDefault();
        let index = $(this).attr('data-index');
        if (index) {
            self.removeInsuranceForm(index);
        }
    });

    $('#addFlightDropdownMenuButton + [aria-labelledby="addFlightDropdownMenuButton"] a').click(function (e) {
        e.preventDefault();
        let val = $(this).attr('data-val');
        if (val) {
            self.addBookingForm(val);
        }
    });

    $('#addHomeDropdownMenuButton + [aria-labelledby="addHomeDropdownMenuButton"] a').click(function (e) {
        e.preventDefault();
        let val = $(this).attr('data-val');
        if (val) {
            self.addBookingForm(val);
        }
    });

    $('#addInsuranceButton').click(function (e) {
        e.preventDefault();
        self.addInsuranceForm();
    });


    $("#submitForm").click(function (e) {
        e.preventDefault();
        self.submitForm();
    });

    self.addPersonForm();
};

ChangeBookingTaskPage.prototype.addPersonForm = function () {
    let self = this;
    let $personForms = $("#personForms");
    this.addFormLoadingShow();
    $.ajax({
        url: '/booking/get/person/form/',
        type: 'get',
        success: function (htmlForm) {
            let $formWrapper = $(htmlForm);
            let $form = $formWrapper.find('form').first();
            $personForms.append($formWrapper);
            self.calculatePersonFormIndexes();
            $form.validate();
            self.showPersonDateOfBirth();
            self.addFormLoadingHide();
        },
        error: function (xhr) {
            alert(xhr.responseText);
            self.addFormLoadingHide();
        }
    });
};

ChangeBookingTaskPage.prototype.removePersonForm = function (index) {
    let $personForms = $("#personForms");

    if ($personForms.find('.card').length === 1) {
        alert('Should be at least one person');
    } else {
        $personForms.find('.card[data-index='+index+']').remove();
        this.calculatePersonFormIndexes();
    }
};

ChangeBookingTaskPage.prototype.calculatePersonFormIndexes = function () {
    let index = 0;
    $("#personForms .card").each(function () {
        $(this).attr('data-index', index);
        $(this).find('.card-header').html('Person '+index+'<button type="button" class="close" data-index="'+index+'" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        index += 1;
    });
};

ChangeBookingTaskPage.prototype.addInsuranceForm = function () {
    let self = this;
    let $insuranceForms = $("#insuranceForms");
    this.addFormLoadingShow();
    $.ajax({
        url: '/booking/get/insurance/form/',
        type: 'get',
        success: function (htmlForm) {
            let $formWrapper = $(htmlForm);
            let $form = $formWrapper.find('form').first();
            $insuranceForms.append($formWrapper);
            $form.find('.input-daterange').datepicker({
                startDate: "dateToday",
                todayHighlight: true
            });
            self.calculateInsuranceFormIndexes();
            $form.validate();
            self.showPersonDateOfBirth();
            self.autoFillFromPreviousForm($form);
            let $firstPersonForm = $("#personForms").find("form[data-type='person']").first();

            if ($firstPersonForm) {
                let personFirstName = $firstPersonForm.find("input[name='first_name']").val();
                let personLastName = $firstPersonForm.find("input[name='last_name']").val();
                if (personFirstName && personLastName) {
                    $form.find("input[name='policyholder']").val(personLastName+' '+personFirstName);
                }
            }
            self.addFormLoadingHide();
        },
        error: function (xhr) {
            alert(xhr.responseText);
            self.addFormLoadingHide();
        }
    });
};

ChangeBookingTaskPage.prototype.calculateInsuranceFormIndexes = function () {
    let index = 0;
    $("#insuranceForms .card").each(function () {
        $(this).attr('data-index', index);
        $(this).find('.card-header button.close').remove();
        $(this).find('.card-header').append('<button type="button" class="close" data-index="'+index+'" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        index += 1;
    });
};

ChangeBookingTaskPage.prototype.showPersonDateOfBirth = function () {
    $("#personForms .card form .date_of_birth_row").each(function () {
        let $date_of_birth_month = $(this).find('select[name="date_of_birth_month"]');
        let $date_of_birth_day = $(this).find('select[name="date_of_birth_day"]');
        let $date_of_birth_year = $(this).find('select[name="date_of_birth_year"]');
        if ($("#insuranceForms .card").length > 0) {
            $(this).show();
            $date_of_birth_month.rules("add", {
                valueNotEquals: '0',
                messages: {
                    valueNotEquals: "Select month",
                }
            });
            $date_of_birth_day.rules("add", {
              valueNotEquals: '0',
              messages: {
                valueNotEquals: "Select day",
              }
            });
            $date_of_birth_year.rules("add", {
              valueNotEquals: '0',
              messages: {
                valueNotEquals: "Select year",
              }
            });

        } else {
            $(this).hide();
        }
    });
};

ChangeBookingTaskPage.prototype.removeInsuranceForm = function (index) {
    let $insuranceForms = $("#insuranceForms");

    $insuranceForms.find('.card[data-index='+index+']').remove();
    this.calculateInsuranceFormIndexes();
    this.showPersonDateOfBirth();
};

ChangeBookingTaskPage.prototype.addBookingForm = function (keyName) {
    let self = this;
    let $chainForms = $("#chainForms");
    this.addFormLoadingShow();
    $.ajax({
        url: '/booking/get/booking/form/',
        type: 'get',
        data: {
            key_name: keyName
        },
        success: function (htmlForm) {
            let $formWrapper = $(htmlForm);
            let $form = $formWrapper.find('form').first();
            $chainForms.append($formWrapper);
            $form.find('.input-daterange').datepicker({
                startDate: "dateToday",
                todayHighlight: true
            });
            self.calculateChainFormIndexes();
            $form.validate();
            self.autoFillFromPreviousForm($form);
            self.addFormLoadingHide();
        },
        error: function (xhr) {
            alert(xhr.responseText);
            self.addFormLoadingHide();
        }
    });
};

ChangeBookingTaskPage.prototype.autoFillFromPreviousForm = function ($form) {
    let $previousForm = $("#chainForms").find("form[data-type='booking_registry']").first();

    if ($previousForm) {
        let previousDateFromVal = $previousForm.find("input[name='date_from']").val();
        if (previousDateFromVal) {
            $form.find("input[name='date_from']").val(previousDateFromVal).datepicker('update');
        }

        let previousDateToVal = $previousForm.find("input[name='date_to']").val();
        if (previousDateToVal) {
            $form.find("input[name='date_to']").val(previousDateToVal).datepicker('update');
        }
    }
};

ChangeBookingTaskPage.prototype.removeChainForm = function (index) {
    let $chainForms = $("#chainForms");

    $chainForms.find('.card[data-index='+index+']').remove();
    this.calculatePersonFormIndexes();
};

ChangeBookingTaskPage.prototype.calculateChainFormIndexes = function () {
    let index = 0;
    $("#chainForms .card").each(function () {
        $(this).attr('data-index', index);
        $(this).find('.card-header button.close').remove();
        $(this).find('.card-header').append('<button type="button" class="close" data-index="'+index+'" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        index += 1;
    });
};

ChangeBookingTaskPage.prototype.addFormLoadingShow = function () {
    $("#addPersonButton").attr('disabled', true);
    $("#addFlightDropdownMenuButton").attr('disabled', true);
    $("#addHomeDropdownMenuButton").attr('disabled', true);
    $("#submitForm").attr('disabled', true);
    $("#add-form-loader").show();
};

ChangeBookingTaskPage.prototype.addFormLoadingHide = function () {
    $("#addPersonButton").attr('disabled', false);
    $("#addFlightDropdownMenuButton").attr('disabled', false);
    $("#addHomeDropdownMenuButton").attr('disabled', false);
    $("#submitForm").attr('disabled', false);
    $("#add-form-loader").hide();
};

ChangeBookingTaskPage.prototype.submitForm = function () {
    let allFormsAreValid = true;
    let $personForms = $("#personForms");
    let $chainForms = $("#chainForms");
    let $insuranceForms = $("#insuranceForms");
    let persons = [];
    let formattedData = {
        'booking_task': {},
        'booking_registry': [],
        'insurances': []
    };

    $personForms.find("form[data-type='person']").each(function () {
        let formData = {};
        if (!$(this).valid()) {
            allFormsAreValid = false;
        } else {
            let formArray = $(this).serializeArray();
            for (let i = 0; i < formArray.length; i++) {
                formData[formArray[i]['name']] = formArray[i]['value'];
            }

            let date_of_birth_month = parseInt($(this).find('select[name="date_of_birth_month"]').val());
            let date_of_birth_day = parseInt($(this).find('select[name="date_of_birth_day"]').val());
            let date_of_birth_year = parseInt($(this).find('select[name="date_of_birth_year"]').val());

            if (date_of_birth_month === 0) {
                date_of_birth_month = 1;
            }

            if (date_of_birth_day === 0) {
                date_of_birth_day = 1;
            }

            if (date_of_birth_year === 0 ) {
                date_of_birth_year = 1945;
            }

            formData['date_of_birth'] = date_of_birth_month + '/' + date_of_birth_day + '/' + date_of_birth_year;
            persons.push(formData);
        }
    });

    $chainForms.find("form[data-type='booking_registry']").each(function () {
        if (!$(this).valid()) {
            allFormsAreValid = false;
        } else {
            let formData = {};
            let formArray = $(this).serializeArray();
            for (let i = 0; i < formArray.length; i++) {
                formData[formArray[i]['name']] = formArray[i]['value'];
            }
            formData['persons'] = persons;
            formattedData['booking_registry'].push(formData);
        }
    });

    $insuranceForms.find("form[data-type='insurance']").each(function () {
        if (!$(this).valid()) {
            allFormsAreValid = false;
        } else {
            let formData = {};
            let formArray = $(this).serializeArray();
            for (let i = 0; i < formArray.length; i++) {
                formData[formArray[i]['name']] = formArray[i]['value'];
            }
            formData['persons'] = persons;
            formattedData['insurances'].push(formData);
        }
    });

    if (allFormsAreValid) {
        if (!formattedData['booking_registry'].length &&
            !formattedData['insurances'].length) {
            alert('There are should be at least one flight, home booking or insurance task');
            return;
        }

        $("#main-form-loader").show();
        $("#main-form-wrapper").hide();

        $.ajax({
            url: '/booking/create/task/',
            type: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(formattedData),
            dataType: "json",
            success: function () {
                $("#main-form-loader").hide();
                $("#main-form-success").show();
                setTimeout(function () {
                    window.location.replace("/booking/");
                }, 1500);
            },
            error: function (xhr) {
                $("#main-form-loader").hide();
                $("#main-form-wrapper").show();
                alert(xhr.responseText);
            }
        });

    }

};

let changeBookingTaskPage = new ChangeBookingTaskPage();
changeBookingTaskPage.init();
