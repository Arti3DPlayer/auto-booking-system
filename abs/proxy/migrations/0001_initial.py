# Generated by Django 2.0.5 on 2018-05-28 20:13

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Proxy',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('login', models.CharField(max_length=255)),
                ('password', models.CharField(max_length=255)),
                ('ip_address', models.GenericIPAddressField()),
                ('port', models.IntegerField()),
                ('status', models.PositiveSmallIntegerField(choices=[(0, 'Working'), (1, 'Not working')], default=0)),
                ('last_check_at', models.DateTimeField(auto_now_add=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
