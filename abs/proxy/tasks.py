import requests

from celery.utils.log import get_task_logger
from django.conf import settings
from abs.taskapp.celery import app

from .models import Proxy

logger = get_task_logger(__name__)


@app.task(bind=True, name='check_proxy')
def check_proxy_task(task):
    logger.info('Start proxy checker')
    proxies = Proxy.objects.filter(proxy_type=Proxy.PROXY_TYPE_SOCKS5)
    for (i, proxy) in enumerate(proxies):
        try:
            resp = requests.get('http://google.com',
                                timeout=6.1,
                                proxies=dict(http=str(proxy),
                                             https=str(proxy)))
            resp.raise_for_status()
            proxy.status = Proxy.WORKING_STATUS
        except requests.RequestException:
            proxy.status = Proxy.NOT_WORKING_STATUS

        proxy.save()

