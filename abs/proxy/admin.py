from django.urls import path
from django.contrib import admin

from .models import Proxy
from .views import run_proxy_checker_view


@admin.register(Proxy)
class ProxyAdmin(admin.ModelAdmin):
    change_list_template = 'admin/proxy/proxy/change_list.html'
    list_display = ('ip_address',
                    'port',
                    'login',
                    'password',
                    'proxy_type',
                    'country_code',
                    'status',
                    'last_check_at',
                    'created_at')
    list_filter = ('proxy_type', 'status')
    search_fields = ('ip_address', )

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('run_proxy_checker/',
                 self.admin_site.admin_view(run_proxy_checker_view),
                 name='run_proxy_checker'),
        ]
        return my_urls + urls
