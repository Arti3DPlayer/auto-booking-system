from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponseRedirect

from .tasks import check_proxy_task


def run_proxy_checker_view(request):
    check_proxy_task.delay()
    messages.add_message(request, messages.INFO, 'Task for checking proxies started')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
