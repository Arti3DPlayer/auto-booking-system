from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.utils.translation import ugettext_lazy as _


class Proxy(models.Model):
    WORKING_STATUS = 0
    NOT_WORKING_STATUS = 1

    PROXY_TYPE_WEB = 0
    PROXY_TYPE_SOCKS4 = 1
    PROXY_TYPE_SOCKS5 = 2

    PROXY_SOURCE_DIDSOFT = 0
    PROXY_SOURCE_VIRTY = 1

    STATUS_CHOICES = (
        (WORKING_STATUS, _(u'Working')),
        (NOT_WORKING_STATUS, _(u'Not working'))
    )

    PROXY_TYPE_CHOICES = (
        (PROXY_TYPE_WEB, _('Web')),
        (PROXY_TYPE_SOCKS4, _(u'Socks4')),
        (PROXY_TYPE_SOCKS5, _(u'Socks5'))
    )

    PROXY_SOURCE_CHOICES = (
        (PROXY_SOURCE_DIDSOFT, _('didsoft.com')),
        (PROXY_SOURCE_VIRTY, _('virty.io'))
    )

    login = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    ip_address = models.GenericIPAddressField()
    port = models.IntegerField()
    proxy_type = models.PositiveSmallIntegerField(choices=PROXY_TYPE_CHOICES,
                                                  default=PROXY_TYPE_WEB)
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES,
                                              default=WORKING_STATUS)
    proxy_source = models.PositiveSmallIntegerField(choices=PROXY_SOURCE_CHOICES,
                                                    default=PROXY_SOURCE_DIDSOFT)
    country_code = models.CharField(max_length=30, blank=True)
    last_check_at = models.DateTimeField(auto_now_add=True, editable=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    @property
    def proxy_type_format(self):
        proxy_type_map = {
            self.PROXY_TYPE_WEB: 'http',
            self.PROXY_TYPE_SOCKS4: 'socks4',
            self.PROXY_TYPE_SOCKS5: 'socks5'
        }
        return proxy_type_map[self.proxy_type]

    def __str__(self):
        return f'{self.proxy_type_format}://{self.login}:{self.password}@{self.ip_address}:{self.port}'
